﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wgsn.Models
{
    public class TaggedProducts
    {
        public string CreatedDate { get; set; }
        public string NoofRetailer { get; set; }
        public string EntitiesTagged { get; set; }
        public string TaxonomyTagged { get; set; }
        public string Outofscope { get; set; }
        public string TotalTaggingCount { get; set; }
        public string TotalOOs { get; set; }
    }

    public class ScreenShot
    {
        public int regionId { get; set; }
        public int userId { get; set; }
        public string batchDate { get; set; }
        public bool isActive { get; set; }
        public string originalFileName { get; set; }
    }

}