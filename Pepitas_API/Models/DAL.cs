﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Net.Mail;

namespace Wgsn.Models
{
    public class DAL
    {
        SqlConnection con = null;
        SqlCommand cmd = null;
        SqlDataAdapter da = null;
        DataSet ds = null;
        public DAL()
        {
            try
            {
                con = new SqlConnection();
                con.ConnectionString = ConfigurationManager.ConnectionStrings["dbConnection"].ConnectionString;
            }
            catch
            {

            }
        }

        public string ProcedureName
        {
            get
            {
                return "";
            }
            set
            {
                cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = con;
                cmd.CommandTimeout = 0;
                cmd.CommandText = value;
            }
        }

        public DataSet ExcuteProcedure
        {
            get
            {
                ds = new DataSet();
                try
                {
                    if (con.State == ConnectionState.Closed)
                        con.Open();
                    da = new SqlDataAdapter(cmd);
                    da.Fill(ds);
                }
                catch (Exception e)
                {
                    var error = "Error :" + e.Message;
                }
                finally
                {
                    if (con.State == ConnectionState.Open)
                        con.Close();
                }
                return ds;
            }
        }

        public void AddParameter(string Name, object Value)
        {
            Name = Name[0] == '@' ? Name : ("@" + Name);
            cmd.Parameters.Add(new SqlParameter(Name, Value));
        }

    }

    public class DBLib
    {
        DAL dal = new DAL();

        #region User Master
        public DataSet SetUserMaster(int user_id, string employee_name, string employee_id, int role_id, string user_name, string user_password, bool is_active, bool is_delete, int uid)
        {
            dal.ProcedureName = "MRS_User";
            dal.AddParameter("user_id", user_id);
            dal.AddParameter("employee_name", employee_name);
            dal.AddParameter("employee_id", employee_id);
            dal.AddParameter("role_id", role_id);
            dal.AddParameter("user_name", user_name);
            dal.AddParameter("user_password", user_password);
            dal.AddParameter("is_active", is_active);
            dal.AddParameter("is_delete", is_delete);
            dal.AddParameter("uid", uid);
            return dal.ExcuteProcedure;
        }
        #endregion

    }
}