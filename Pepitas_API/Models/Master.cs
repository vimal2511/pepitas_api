﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wgsn.Models
{
    public class Region
    {
        public int Region_Id { get; set; }
        public string Region_Name { get; set; }
        public string Region_Code { get; set; }
        public bool Region_Active { get; set; }
        public bool Region_Delete { get; set; }
    }

    public class UserMaster
    {
        public int user_id { get; set; }
        public string employee_name { get; set; }
        public string employee_id { get; set; }
        public int role_id { get; set; }
        public string user_name { get; set; }
        public string user_password { get; set; }
        public bool is_active { get; set; }
        public bool is_delete { get; set; }
    }

    public class Mapping_Data_Temp
    {
        public int ID { get; set; }
        public Nullable<int> region_id { get; set; }
        public Nullable<int> no_of_agent { get; set; }
        public string agent_ids { get; set; }
        public Nullable<System.DateTime> Created_date { get; set; }
        public Nullable<int> retailer_size { get; set; }
        public int Isreallocate { get; set; }
    }
}