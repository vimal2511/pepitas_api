﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wgsn.Models
{
    public class HomeRegion
    {
        public string Region { get;set;}
        public int CurrentBatchDate { get; set; }
        public int PreviousBatchDate { get; set; }
    }

    public class Login
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }

    public class LoginResult
    {
        public string Message { get; set; }
        public bool Clear { get; set; }
        public int UserId { get; set; }
        public int RoleId { get; set; }
        public string UserName { get; set; }
        public string RoleName { get; set; }
        public string Exception { get; set; }
    }

    public partial class WokflowSpilit
    {
        public string batchDate { get; set; }
        public string[] UserId { get; set; }
        public string ISpriority { get; set; }
    }
}