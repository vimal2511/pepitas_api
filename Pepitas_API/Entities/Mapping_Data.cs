//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Pepitas_API.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class Mapping_Data
    {
        public int ID { get; set; }
        public Nullable<int> region_id { get; set; }
        public Nullable<int> no_of_agent { get; set; }
        public string agent_ids { get; set; }
        public Nullable<System.DateTime> Created_date { get; set; }
        public Nullable<int> retailer_size { get; set; }
        public Nullable<int> small_count { get; set; }
        public Nullable<int> medium_count { get; set; }
        public Nullable<int> big_count { get; set; }
        public Nullable<int> verybig_count { get; set; }
    }
}
