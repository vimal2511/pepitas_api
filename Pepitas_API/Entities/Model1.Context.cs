﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Pepitas_API.Entities
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.Core.Objects;
    using System.Linq;
    
    public partial class PEPITASEntities : DbContext
    {
        public PEPITASEntities()
            : base("name=PEPITASEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<Attribute_Inflow> Attribute_Inflow { get; set; }
        public virtual DbSet<Country_Agent_Map> Country_Agent_Map { get; set; }
        public virtual DbSet<Pip_MasterDetails> Pip_MasterDetails { get; set; }
        public virtual DbSet<screen_shot> screen_shot { get; set; }
        public virtual DbSet<user_master> user_master { get; set; }
        public virtual DbSet<Extra_task_list> Extra_task_list { get; set; }
        public virtual DbSet<Mapping_Data> Mapping_Data { get; set; }
        public virtual DbSet<retailer_range_master> retailer_range_master { get; set; }
        public virtual DbSet<Pip_Ticket_details> Pip_Ticket_details { get; set; }
        public virtual DbSet<region_list> region_list { get; set; }
        public virtual DbSet<BreakTime> BreakTimes { get; set; }
        public virtual DbSet<Extra_task> Extra_task { get; set; }
        public virtual DbSet<priority_retailer> priority_retailer { get; set; }
        public virtual DbSet<TargetSpilit> TargetSpilits { get; set; }
        public virtual DbSet<RetailerGroupAllocation> RetailerGroupAllocations { get; set; }
    
        [DbFunction("PEPITASEntities", "SplitString")]
        public virtual IQueryable<SplitString_Result> SplitString(string input, string character)
        {
            var inputParameter = input != null ?
                new ObjectParameter("Input", input) :
                new ObjectParameter("Input", typeof(string));
    
            var characterParameter = character != null ?
                new ObjectParameter("Character", character) :
                new ObjectParameter("Character", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.CreateQuery<SplitString_Result>("[PEPITASEntities].[SplitString](@Input, @Character)", inputParameter, characterParameter);
        }
    
        [DbFunction("PEPITASEntities", "SplitStrings")]
        public virtual IQueryable<SplitStrings_Result> SplitStrings(string input, string character)
        {
            var inputParameter = input != null ?
                new ObjectParameter("Input", input) :
                new ObjectParameter("Input", typeof(string));
    
            var characterParameter = character != null ?
                new ObjectParameter("Character", character) :
                new ObjectParameter("Character", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.CreateQuery<SplitStrings_Result>("[PEPITASEntities].[SplitStrings](@Input, @Character)", inputParameter, characterParameter);
        }
    
        public virtual ObjectResult<Fetch_Attribute_Inflow_Result> Fetch_Attribute_Inflow(string region, Nullable<System.DateTime> fromDate, Nullable<System.DateTime> toDate)
        {
            var regionParameter = region != null ?
                new ObjectParameter("Region", region) :
                new ObjectParameter("Region", typeof(string));
    
            var fromDateParameter = fromDate.HasValue ?
                new ObjectParameter("FromDate", fromDate) :
                new ObjectParameter("FromDate", typeof(System.DateTime));
    
            var toDateParameter = toDate.HasValue ?
                new ObjectParameter("ToDate", toDate) :
                new ObjectParameter("ToDate", typeof(System.DateTime));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Fetch_Attribute_Inflow_Result>("Fetch_Attribute_Inflow", regionParameter, fromDateParameter, toDateParameter);
        }
    
        public virtual ObjectResult<Get_Attribute_Inflow_Result> Get_Attribute_Inflow(Nullable<System.DateTime> currentDate, Nullable<System.DateTime> previousDate)
        {
            var currentDateParameter = currentDate.HasValue ?
                new ObjectParameter("currentDate", currentDate) :
                new ObjectParameter("currentDate", typeof(System.DateTime));
    
            var previousDateParameter = previousDate.HasValue ?
                new ObjectParameter("previousDate", previousDate) :
                new ObjectParameter("previousDate", typeof(System.DateTime));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Get_Attribute_Inflow_Result>("Get_Attribute_Inflow", currentDateParameter, previousDateParameter);
        }
    
        public virtual int Home_Page_Region_Wise_Report(string currentBatchDate, string previousBatchDate)
        {
            var currentBatchDateParameter = currentBatchDate != null ?
                new ObjectParameter("CurrentBatchDate", currentBatchDate) :
                new ObjectParameter("CurrentBatchDate", typeof(string));
    
            var previousBatchDateParameter = previousBatchDate != null ?
                new ObjectParameter("PreviousBatchDate", previousBatchDate) :
                new ObjectParameter("PreviousBatchDate", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("Home_Page_Region_Wise_Report", currentBatchDateParameter, previousBatchDateParameter);
        }
    
        public virtual ObjectResult<MRG_Attribute_inflow_Result> MRG_Attribute_inflow(string region_value, string attribute, string from_date, string to_date)
        {
            var region_valueParameter = region_value != null ?
                new ObjectParameter("region_value", region_value) :
                new ObjectParameter("region_value", typeof(string));
    
            var attributeParameter = attribute != null ?
                new ObjectParameter("attribute", attribute) :
                new ObjectParameter("attribute", typeof(string));
    
            var from_dateParameter = from_date != null ?
                new ObjectParameter("from_date", from_date) :
                new ObjectParameter("from_date", typeof(string));
    
            var to_dateParameter = to_date != null ?
                new ObjectParameter("To_date", to_date) :
                new ObjectParameter("To_date", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<MRG_Attribute_inflow_Result>("MRG_Attribute_inflow", region_valueParameter, attributeParameter, from_dateParameter, to_dateParameter);
        }
    
        public virtual ObjectResult<MRG_Regionlist_Result> MRG_Regionlist(Nullable<int> region_active)
        {
            var region_activeParameter = region_active.HasValue ?
                new ObjectParameter("region_active", region_active) :
                new ObjectParameter("region_active", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<MRG_Regionlist_Result>("MRG_Regionlist", region_activeParameter);
        }
    
        public virtual ObjectResult<MRG_User_Result> MRG_User(Nullable<int> viewType)
        {
            var viewTypeParameter = viewType.HasValue ?
                new ObjectParameter("ViewType", viewType) :
                new ObjectParameter("ViewType", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<MRG_User_Result>("MRG_User", viewTypeParameter);
        }
    
        public virtual ObjectResult<MRS_RegionList_Result> MRS_RegionList(Nullable<int> region_id, string region_name, string region_Code, Nullable<bool> is_active, Nullable<bool> is_delete, Nullable<int> user_id)
        {
            var region_idParameter = region_id.HasValue ?
                new ObjectParameter("region_id", region_id) :
                new ObjectParameter("region_id", typeof(int));
    
            var region_nameParameter = region_name != null ?
                new ObjectParameter("region_name", region_name) :
                new ObjectParameter("region_name", typeof(string));
    
            var region_CodeParameter = region_Code != null ?
                new ObjectParameter("region_Code", region_Code) :
                new ObjectParameter("region_Code", typeof(string));
    
            var is_activeParameter = is_active.HasValue ?
                new ObjectParameter("is_active", is_active) :
                new ObjectParameter("is_active", typeof(bool));
    
            var is_deleteParameter = is_delete.HasValue ?
                new ObjectParameter("is_delete", is_delete) :
                new ObjectParameter("is_delete", typeof(bool));
    
            var user_idParameter = user_id.HasValue ?
                new ObjectParameter("user_id", user_id) :
                new ObjectParameter("user_id", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<MRS_RegionList_Result>("MRS_RegionList", region_idParameter, region_nameParameter, region_CodeParameter, is_activeParameter, is_deleteParameter, user_idParameter);
        }
    
        public virtual ObjectResult<MRS_User_Result> MRS_User(Nullable<int> user_id, string employee_name, string employee_id, Nullable<int> role_id, string user_name, string user_password, Nullable<bool> is_active, Nullable<bool> is_delete, Nullable<int> uid)
        {
            var user_idParameter = user_id.HasValue ?
                new ObjectParameter("user_id", user_id) :
                new ObjectParameter("user_id", typeof(int));
    
            var employee_nameParameter = employee_name != null ?
                new ObjectParameter("employee_name", employee_name) :
                new ObjectParameter("employee_name", typeof(string));
    
            var employee_idParameter = employee_id != null ?
                new ObjectParameter("employee_id", employee_id) :
                new ObjectParameter("employee_id", typeof(string));
    
            var role_idParameter = role_id.HasValue ?
                new ObjectParameter("role_id", role_id) :
                new ObjectParameter("role_id", typeof(int));
    
            var user_nameParameter = user_name != null ?
                new ObjectParameter("user_name", user_name) :
                new ObjectParameter("user_name", typeof(string));
    
            var user_passwordParameter = user_password != null ?
                new ObjectParameter("user_password", user_password) :
                new ObjectParameter("user_password", typeof(string));
    
            var is_activeParameter = is_active.HasValue ?
                new ObjectParameter("is_active", is_active) :
                new ObjectParameter("is_active", typeof(bool));
    
            var is_deleteParameter = is_delete.HasValue ?
                new ObjectParameter("is_delete", is_delete) :
                new ObjectParameter("is_delete", typeof(bool));
    
            var uidParameter = uid.HasValue ?
                new ObjectParameter("uid", uid) :
                new ObjectParameter("uid", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<MRS_User_Result>("MRS_User", user_idParameter, employee_nameParameter, employee_idParameter, role_idParameter, user_nameParameter, user_passwordParameter, is_activeParameter, is_deleteParameter, uidParameter);
        }
    
        public virtual ObjectResult<MRS_UserChangePassword_Result> MRS_UserChangePassword(Nullable<int> user_id, string user_password)
        {
            var user_idParameter = user_id.HasValue ?
                new ObjectParameter("user_id", user_id) :
                new ObjectParameter("user_id", typeof(int));
    
            var user_passwordParameter = user_password != null ?
                new ObjectParameter("user_password", user_password) :
                new ObjectParameter("user_password", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<MRS_UserChangePassword_Result>("MRS_UserChangePassword", user_idParameter, user_passwordParameter);
        }
    
        public virtual ObjectResult<Region_Wise_count_Result> Region_Wise_count(string currentBatchDate)
        {
            var currentBatchDateParameter = currentBatchDate != null ?
                new ObjectParameter("CurrentBatchDate", currentBatchDate) :
                new ObjectParameter("CurrentBatchDate", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Region_Wise_count_Result>("Region_Wise_count", currentBatchDateParameter);
        }
    
        public virtual ObjectResult<Region_wise_insert_count_Result> Region_wise_insert_count(string currentBatchDate, Nullable<int> userId)
        {
            var currentBatchDateParameter = currentBatchDate != null ?
                new ObjectParameter("CurrentBatchDate", currentBatchDate) :
                new ObjectParameter("CurrentBatchDate", typeof(string));
    
            var userIdParameter = userId.HasValue ?
                new ObjectParameter("UserId", userId) :
                new ObjectParameter("UserId", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Region_wise_insert_count_Result>("Region_wise_insert_count", currentBatchDateParameter, userIdParameter);
        }
    
        public virtual int Save_Pip_MasterDetails(Nullable<System.DateTime> createdDate, Nullable<System.DateTime> batchDate, string dept_Market, string region, Nullable<int> category, Nullable<int> subCategory, Nullable<int> brands, Nullable<int> prints, Nullable<int> materials, Nullable<int> colors, Nullable<int> readyToPost, Nullable<int> live)
        {
            var createdDateParameter = createdDate.HasValue ?
                new ObjectParameter("CreatedDate", createdDate) :
                new ObjectParameter("CreatedDate", typeof(System.DateTime));
    
            var batchDateParameter = batchDate.HasValue ?
                new ObjectParameter("BatchDate", batchDate) :
                new ObjectParameter("BatchDate", typeof(System.DateTime));
    
            var dept_MarketParameter = dept_Market != null ?
                new ObjectParameter("Dept_Market", dept_Market) :
                new ObjectParameter("Dept_Market", typeof(string));
    
            var regionParameter = region != null ?
                new ObjectParameter("Region", region) :
                new ObjectParameter("Region", typeof(string));
    
            var categoryParameter = category.HasValue ?
                new ObjectParameter("Category", category) :
                new ObjectParameter("Category", typeof(int));
    
            var subCategoryParameter = subCategory.HasValue ?
                new ObjectParameter("SubCategory", subCategory) :
                new ObjectParameter("SubCategory", typeof(int));
    
            var brandsParameter = brands.HasValue ?
                new ObjectParameter("Brands", brands) :
                new ObjectParameter("Brands", typeof(int));
    
            var printsParameter = prints.HasValue ?
                new ObjectParameter("Prints", prints) :
                new ObjectParameter("Prints", typeof(int));
    
            var materialsParameter = materials.HasValue ?
                new ObjectParameter("Materials", materials) :
                new ObjectParameter("Materials", typeof(int));
    
            var colorsParameter = colors.HasValue ?
                new ObjectParameter("Colors", colors) :
                new ObjectParameter("Colors", typeof(int));
    
            var readyToPostParameter = readyToPost.HasValue ?
                new ObjectParameter("ReadyToPost", readyToPost) :
                new ObjectParameter("ReadyToPost", typeof(int));
    
            var liveParameter = live.HasValue ?
                new ObjectParameter("Live", live) :
                new ObjectParameter("Live", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("Save_Pip_MasterDetails", createdDateParameter, batchDateParameter, dept_MarketParameter, regionParameter, categoryParameter, subCategoryParameter, brandsParameter, printsParameter, materialsParameter, colorsParameter, readyToPostParameter, liveParameter);
        }
    
        public virtual ObjectResult<Sp_getmaster_details_Result> Sp_getmaster_details(string createdDate, Nullable<int> userId)
        {
            var createdDateParameter = createdDate != null ?
                new ObjectParameter("CreatedDate", createdDate) :
                new ObjectParameter("CreatedDate", typeof(string));
    
            var userIdParameter = userId.HasValue ?
                new ObjectParameter("UserId", userId) :
                new ObjectParameter("UserId", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Sp_getmaster_details_Result>("Sp_getmaster_details", createdDateParameter, userIdParameter);
        }
    
        public virtual ObjectResult<Sp_getReport_details_Result> Sp_getReport_details(string createdDate)
        {
            var createdDateParameter = createdDate != null ?
                new ObjectParameter("CreatedDate", createdDate) :
                new ObjectParameter("CreatedDate", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Sp_getReport_details_Result>("Sp_getReport_details", createdDateParameter);
        }
    
        public virtual ObjectResult<Sp_UserTagged_Products_Result> Sp_UserTagged_Products(string fromDate, string to, string userId)
        {
            var fromDateParameter = fromDate != null ?
                new ObjectParameter("FromDate", fromDate) :
                new ObjectParameter("FromDate", typeof(string));
    
            var toParameter = to != null ?
                new ObjectParameter("To", to) :
                new ObjectParameter("To", typeof(string));
    
            var userIdParameter = userId != null ?
                new ObjectParameter("UserId", userId) :
                new ObjectParameter("UserId", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Sp_UserTagged_Products_Result>("Sp_UserTagged_Products", fromDateParameter, toParameter, userIdParameter);
        }
    
        public virtual ObjectResult<Sp_UsertaggedExport_products_Result> Sp_UsertaggedExport_products(string fromDate, string to, string userId)
        {
            var fromDateParameter = fromDate != null ?
                new ObjectParameter("FromDate", fromDate) :
                new ObjectParameter("FromDate", typeof(string));
    
            var toParameter = to != null ?
                new ObjectParameter("To", to) :
                new ObjectParameter("To", typeof(string));
    
            var userIdParameter = userId != null ?
                new ObjectParameter("UserId", userId) :
                new ObjectParameter("UserId", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Sp_UsertaggedExport_products_Result>("Sp_UsertaggedExport_products", fromDateParameter, toParameter, userIdParameter);
        }
    
        public virtual int Sp_UsertaggedExport_products_v1(string fromDate, string to, string userId)
        {
            var fromDateParameter = fromDate != null ?
                new ObjectParameter("FromDate", fromDate) :
                new ObjectParameter("FromDate", typeof(string));
    
            var toParameter = to != null ?
                new ObjectParameter("To", to) :
                new ObjectParameter("To", typeof(string));
    
            var userIdParameter = userId != null ?
                new ObjectParameter("UserId", userId) :
                new ObjectParameter("UserId", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("Sp_UsertaggedExport_products_v1", fromDateParameter, toParameter, userIdParameter);
        }
    
        public virtual int Usp__insert_update_pip_masterdetails()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("Usp__insert_update_pip_masterdetails");
        }
    
        public virtual int USP_ActiveInactive_Region(string regionid, Nullable<int> region_sts)
        {
            var regionidParameter = regionid != null ?
                new ObjectParameter("regionid", regionid) :
                new ObjectParameter("regionid", typeof(string));
    
            var region_stsParameter = region_sts.HasValue ?
                new ObjectParameter("region_sts", region_sts) :
                new ObjectParameter("region_sts", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("USP_ActiveInactive_Region", regionidParameter, region_stsParameter);
        }
    
        public virtual ObjectResult<string> USP_ADD_REGION(string regionCode, string regionName, Nullable<int> no_of_agent)
        {
            var regionCodeParameter = regionCode != null ?
                new ObjectParameter("regionCode", regionCode) :
                new ObjectParameter("regionCode", typeof(string));
    
            var regionNameParameter = regionName != null ?
                new ObjectParameter("regionName", regionName) :
                new ObjectParameter("regionName", typeof(string));
    
            var no_of_agentParameter = no_of_agent.HasValue ?
                new ObjectParameter("no_of_agent", no_of_agent) :
                new ObjectParameter("no_of_agent", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<string>("USP_ADD_REGION", regionCodeParameter, regionNameParameter, no_of_agentParameter);
        }
    
        public virtual ObjectResult<USP_Get_screenshot_Result> USP_Get_screenshot(Nullable<int> userid)
        {
            var useridParameter = userid.HasValue ?
                new ObjectParameter("userid", userid) :
                new ObjectParameter("userid", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<USP_Get_screenshot_Result>("USP_Get_screenshot", useridParameter);
        }
    
        public virtual ObjectResult<usp_getretailer_Result> usp_getretailer(string createdDate, Nullable<int> userid, Nullable<int> region_Id)
        {
            var createdDateParameter = createdDate != null ?
                new ObjectParameter("CreatedDate", createdDate) :
                new ObjectParameter("CreatedDate", typeof(string));
    
            var useridParameter = userid.HasValue ?
                new ObjectParameter("userid", userid) :
                new ObjectParameter("userid", typeof(int));
    
            var region_IdParameter = region_Id.HasValue ?
                new ObjectParameter("region_Id", region_Id) :
                new ObjectParameter("region_Id", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<usp_getretailer_Result>("usp_getretailer", createdDateParameter, useridParameter, region_IdParameter);
        }
    
        public virtual int USP_Insert_Break(string master_ID, string break_start, string start_Time, Nullable<int> diff_time)
        {
            var master_IDParameter = master_ID != null ?
                new ObjectParameter("Master_ID", master_ID) :
                new ObjectParameter("Master_ID", typeof(string));
    
            var break_startParameter = break_start != null ?
                new ObjectParameter("Break_start", break_start) :
                new ObjectParameter("Break_start", typeof(string));
    
            var start_TimeParameter = start_Time != null ?
                new ObjectParameter("Start_Time", start_Time) :
                new ObjectParameter("Start_Time", typeof(string));
    
            var diff_timeParameter = diff_time.HasValue ?
                new ObjectParameter("diff_time", diff_time) :
                new ObjectParameter("diff_time", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("USP_Insert_Break", master_IDParameter, break_startParameter, start_TimeParameter, diff_timeParameter);
        }
    
        public virtual int USP_INSERT_Mapping_Data(Nullable<int> region_id, Nullable<int> no_of_agent, string agent_id)
        {
            var region_idParameter = region_id.HasValue ?
                new ObjectParameter("region_id", region_id) :
                new ObjectParameter("region_id", typeof(int));
    
            var no_of_agentParameter = no_of_agent.HasValue ?
                new ObjectParameter("no_of_agent", no_of_agent) :
                new ObjectParameter("no_of_agent", typeof(int));
    
            var agent_idParameter = agent_id != null ?
                new ObjectParameter("agent_id", agent_id) :
                new ObjectParameter("agent_id", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("USP_INSERT_Mapping_Data", region_idParameter, no_of_agentParameter, agent_idParameter);
        }
    
        public virtual int USP_INSERT_Pepitas_data(string retailername, string region, string batchdate, Nullable<int> weightage, Nullable<System.DateTime> createddate, Nullable<int> wrflow_Total, string wrflow_Value)
        {
            var retailernameParameter = retailername != null ?
                new ObjectParameter("retailername", retailername) :
                new ObjectParameter("retailername", typeof(string));
    
            var regionParameter = region != null ?
                new ObjectParameter("region", region) :
                new ObjectParameter("region", typeof(string));
    
            var batchdateParameter = batchdate != null ?
                new ObjectParameter("batchdate", batchdate) :
                new ObjectParameter("batchdate", typeof(string));
    
            var weightageParameter = weightage.HasValue ?
                new ObjectParameter("weightage", weightage) :
                new ObjectParameter("weightage", typeof(int));
    
            var createddateParameter = createddate.HasValue ?
                new ObjectParameter("createddate", createddate) :
                new ObjectParameter("createddate", typeof(System.DateTime));
    
            var wrflow_TotalParameter = wrflow_Total.HasValue ?
                new ObjectParameter("wrflow_Total", wrflow_Total) :
                new ObjectParameter("wrflow_Total", typeof(int));
    
            var wrflow_ValueParameter = wrflow_Value != null ?
                new ObjectParameter("wrflow_Value", wrflow_Value) :
                new ObjectParameter("wrflow_Value", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("USP_INSERT_Pepitas_data", retailernameParameter, regionParameter, batchdateParameter, weightageParameter, createddateParameter, wrflow_TotalParameter, wrflow_ValueParameter);
        }
    
        public virtual int USP_INSERT_SCREENSHOT(Nullable<int> reg_id, Nullable<int> user_id, string filename, Nullable<System.DateTime> batch_date, Nullable<bool> is_active)
        {
            var reg_idParameter = reg_id.HasValue ?
                new ObjectParameter("reg_id", reg_id) :
                new ObjectParameter("reg_id", typeof(int));
    
            var user_idParameter = user_id.HasValue ?
                new ObjectParameter("user_id", user_id) :
                new ObjectParameter("user_id", typeof(int));
    
            var filenameParameter = filename != null ?
                new ObjectParameter("filename", filename) :
                new ObjectParameter("filename", typeof(string));
    
            var batch_dateParameter = batch_date.HasValue ?
                new ObjectParameter("batch_date", batch_date) :
                new ObjectParameter("batch_date", typeof(System.DateTime));
    
            var is_activeParameter = is_active.HasValue ?
                new ObjectParameter("is_active", is_active) :
                new ObjectParameter("is_active", typeof(bool));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("USP_INSERT_SCREENSHOT", reg_idParameter, user_idParameter, filenameParameter, batch_dateParameter, is_activeParameter);
        }
    
        public virtual int USP_REALLOCATE()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("USP_REALLOCATE");
        }
    
        public virtual int usp_RegionwiseReport(string currentBatchDate, string previousBatchDate)
        {
            var currentBatchDateParameter = currentBatchDate != null ?
                new ObjectParameter("CurrentBatchDate", currentBatchDate) :
                new ObjectParameter("CurrentBatchDate", typeof(string));
    
            var previousBatchDateParameter = previousBatchDate != null ?
                new ObjectParameter("PreviousBatchDate", previousBatchDate) :
                new ObjectParameter("PreviousBatchDate", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("usp_RegionwiseReport", currentBatchDateParameter, previousBatchDateParameter);
        }
    
        public virtual ObjectResult<usp_getdeleteretailers_Result> usp_getdeleteretailers(string createdDate, Nullable<int> userid)
        {
            var createdDateParameter = createdDate != null ?
                new ObjectParameter("CreatedDate", createdDate) :
                new ObjectParameter("CreatedDate", typeof(string));
    
            var useridParameter = userid.HasValue ?
                new ObjectParameter("userid", userid) :
                new ObjectParameter("userid", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<usp_getdeleteretailers_Result>("usp_getdeleteretailers", createdDateParameter, useridParameter);
        }
    
        public virtual int USP_AgentCountryAllocation()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("USP_AgentCountryAllocation");
        }
    
        public virtual ObjectResult<Sp_UsertaggedExport_products_Details_Result> Sp_UsertaggedExport_products_Details(string createdDate, string todate, string userId)
        {
            var createdDateParameter = createdDate != null ?
                new ObjectParameter("CreatedDate", createdDate) :
                new ObjectParameter("CreatedDate", typeof(string));
    
            var todateParameter = todate != null ?
                new ObjectParameter("todate", todate) :
                new ObjectParameter("todate", typeof(string));
    
            var userIdParameter = userId != null ?
                new ObjectParameter("UserId", userId) :
                new ObjectParameter("UserId", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Sp_UsertaggedExport_products_Details_Result>("Sp_UsertaggedExport_products_Details", createdDateParameter, todateParameter, userIdParameter);
        }
    }
}
