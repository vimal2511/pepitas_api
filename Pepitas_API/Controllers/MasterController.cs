﻿using Newtonsoft.Json;
using Pepitas_API.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
//using System.Web.Mvc;
using Wgsn.Models;
using Newtonsoft.Json.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Core.EntityClient;
using System.Globalization;
using System.Data.Entity;

namespace Pepitas_API.Controllers
{
    public class MasterController : ApiController
    {
        public TimeZoneInfo INDIAN_ZONE = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");
        #region Variable Declaration
        PEPITASEntities conn = new PEPITASEntities();
        #endregion

        #region Region Master

        [Route("api/Master/GetRegion")]
        public string GetRegion()
        {
            var resp = conn.MRG_Regionlist(1).ToList();
            // var resp = conn.user_master.ToList().FirstOrDefault();
            string RESULT = JsonConvert.SerializeObject(resp);
            return RESULT;
        }

        [Route("api/Master/SetRegion")]
        public string SetRegion(Region input)
        {
            var resp = conn.MRS_RegionList(input.Region_Id, input.Region_Name, input.Region_Code, input.Region_Active, input.Region_Delete, 1).FirstOrDefault();
            // JsonSerializer ser = new JsonSerializer();
            string RESULT = JsonConvert.SerializeObject(resp);
            return RESULT;

        }

        #endregion

        #region User Master

        [Route("api/Master/GetUserMaster")]
        public List<MRG_User_Result> GetUserMaster()
        {
            var value = conn.MRG_User(1).ToList();

            var RESULT = value.Where(x => x.role_id != 2).ToList();
            return RESULT;
        }


        [Route("api/Master/GetPriorityListMaster")]
        public List<priority_retailer> GetPriorityListMaster()
        {
            return conn.priority_retailer.Where(x => x.Active == true).ToList();
        }
        [HttpPost]
        [Route("api/Master/GetPriorityInsertUpdate")]
        public string GetPriorityInsertUpdate(priority_retailer input)
        {

            try
            {
                DateTime now = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, INDIAN_ZONE);
                var result = conn.priority_retailer.Where(x => x.Retailer.ToLower() == input.Retailer.ToLower()).FirstOrDefault();
                if (result != null)
                {

                    result.Retailer = input.Retailer;
                    result.Type = input.Type;
                    result.region = input.region;
                    result.UserId = input.UserId;
                    result.CreatedDate = now;
                    result.Active = input.Active;
                    conn.Entry(result).State = System.Data.Entity.EntityState.Modified;
                    conn.SaveChanges();
                }
                else
                {
                    priority_retailer list = new priority_retailer();
                    list.Retailer = input.Retailer;
                    list.region = input.region;
                    list.Type = input.Type;
                    list.UserId = input.UserId;
                    list.CreatedDate = now;
                    list.Active = true;
                    conn.priority_retailer.Add(list);
                    conn.SaveChanges();
                }

                return "Successfully Saved !";
            }
            catch (Exception e)
            {
                return "error: " + e.Message.ToString();
            }
        }
        public class highRetailerList
        {
            public int Id { get; set; }
            public string Retailer { get; set; }
            public string region_code { get; set; }
            public string BatchDate { get; set; }
            public string agentName { get; set; }
            public int wrflow_Total { get; set; }
        }
        public List<highRetailerList> GetPriorityList(string CreatedDate)
        {
            using (PEPITASEntities db1 = new PEPITASEntities())
            {
                DataTable dt = new DataTable();
                DateTime now = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, INDIAN_ZONE);
                CreatedDate = CreatedDate.Split('/')[2] + "-" + CreatedDate.Split('/')[1] + "-" + CreatedDate.Split('/')[0];
                //string batchdate =now.ToString("yyyy-MM-dd");
                HomeAPIController hm = new HomeAPIController();

                using (SqlConnection con = new SqlConnection(hm.getConnection()))
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        con.Open();
                        cmd.CommandText = "select distinct p.Id, p.Retailer,r.region_code,p.BatchDate,p.UserId from Pip_MasterDetails p, priority_retailer p1,region_list r,user_master u where p.Retailer = p1.Retailer and r.region_id = p.region  and BatchDate = '" + CreatedDate + "' and p.UserId in (case when p.UserId = 0 then '-' else u.user_id end)";
                        cmd.CommandType = CommandType.Text;
                        cmd.Connection = con;
                        cmd.Connection = con;
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        da.Fill(dt);
                        con.Close();
                    }
                }
                List<highRetailerList> list = new List<highRetailerList>();
                for (int m = 0; m < dt.Rows.Count; m++)
                {
                    highRetailerList row = new highRetailerList();
                    row.Id = Convert.ToInt32(dt.Rows[m]["Id"].ToString());
                    row.Retailer = dt.Rows[m]["Retailer"].ToString();
                    row.region_code = dt.Rows[m]["region_code"].ToString();
                    row.BatchDate = dt.Rows[m]["BatchDate"].ToString();
                    int userid = Convert.ToInt32(dt.Rows[m]["UserId"].ToString());
                    if (userid == 0)
                        row.agentName = "-";
                    else
                        row.agentName = conn.user_master.Where(x => x.user_id == userid).Select(x => x.user_name).FirstOrDefault();
                    list.Add(row);

                }

                return list;

            }
        }

        [Route("api/Master/SetUserMaster")]
        public string SetUserMaster(UserMaster input)
        {
            var resp = conn.MRS_User(input.user_id, input.employee_name, input.employee_id, input.role_id, input.user_name, input.user_password, input.is_active, input.is_delete, 1).FirstOrDefault();
            JsonSerializer ser = new JsonSerializer();
            string RESULT = JsonConvert.SerializeObject(resp);
            return RESULT;
        }
        #endregion

        #region Ticket Details

        [Route("api/Master/GetTicketDetails")]
        public List<Pip_Ticket_details> GetTicketDetails()
        {
            var RESULT = conn.Pip_Ticket_details.Where(x => x.is_active == true).ToList();
            return RESULT;
        }

        [HttpPut]
        [Route("api/Master/SetTicketDetails")]
        public string SetTicketDetails(Pip_Ticket_details input)
        {
            try
            {
                var RemoveItem = conn.Pip_Ticket_details.Find(input.Id);
                if (RemoveItem != null)
                {
                    RemoveItem.is_active = false;
                    conn.Entry(RemoveItem).State = System.Data.Entity.EntityState.Modified;
                    conn.SaveChanges();
                }

                return "success";
            }
            catch (Exception e)
            {
                return "error: " + e.Message.ToString();
            }
        }

        #endregion

        #region Assign Retailer
        [Route("api/Master/GetAssignRetailer")]
        public List<highRetailerList> GetAssignRetailer(string CreatedDate)
        {
            using (PEPITASEntities db1 = new PEPITASEntities())
            {
                DataTable dt = new DataTable();
                DateTime now = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, INDIAN_ZONE);
                CreatedDate = CreatedDate.Split('/')[2] + "-" + CreatedDate.Split('/')[1] + "-" + CreatedDate.Split('/')[0];
                HomeAPIController hm = new HomeAPIController();
                using (SqlConnection con = new SqlConnection(hm.getConnection()))
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        con.Open();
                        cmd.CommandText = "select distinct p.Id, p.Retailer,r.region_code,p.BatchDate,p.UserId,(select employee_name From user_master us where us.user_id=p.UserId) UserName from Pip_MasterDetails p, priority_retailer p1,region_list r,user_master u where p.Retailer = p1.Retailer and r.region_id = p.region  and BatchDate = '" + CreatedDate + "' and p.UserId in (case when p.UserId = 0 then '-' else u.user_id end)";
                        cmd.CommandType = CommandType.Text;
                        cmd.Connection = con;
                        cmd.Connection = con;
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        da.Fill(dt);
                        con.Close();
                    }
                }
                List<highRetailerList> list = new List<highRetailerList>();
                for (int m = 0; m < dt.Rows.Count; m++)
                {
                    highRetailerList row = new highRetailerList();
                    row.Id = Convert.ToInt32(dt.Rows[m]["Id"].ToString());
                    row.Retailer = dt.Rows[m]["Retailer"].ToString();
                    row.region_code = dt.Rows[m]["region_code"].ToString();
                    row.BatchDate = String.Format("{0:dd/MM/yyyy}", dt.Rows[m]["BatchDate"]);
                    int userid = Convert.ToInt32(dt.Rows[m]["UserId"].ToString());
                    if (userid == 0)
                        row.agentName = "-";
                    else
                        row.agentName = dt.Rows[m]["UserName"].ToString();

                    list.Add(row);
                }
                return list;
            }
        }

        [HttpPost]
        [Route("api/Master/insertUpdateAssinRetailer")]
        public string insertUpdateAssinRetailer(List<priority_retailer> inputs)
        {
            try
            {
                DateTime now = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, INDIAN_ZONE);
                foreach (priority_retailer input in inputs)
                {
                    var result = conn.Pip_MasterDetails.Where(x => x.Id == input.Id).FirstOrDefault();
                    if (result != null)
                    {
                        result.UserId = input.UserId;
                        result.status = 1;
                        result.assigned_time = now;
                        conn.Entry(result).State = System.Data.Entity.EntityState.Modified;
                        conn.SaveChanges();
                    }
                }
                return "Successfully Saved !";
            }
            catch (Exception e)
            {
                return "error: " + e.Message.ToString();
            }
        }
        #endregion


        #region Get Reprocessing Retailer
        [Route("api/Master/GetReprocessingRetailer")]
        public List<highRetailerList> GetReprocessingRetailer(string CreatedDate)
        {
            using (PEPITASEntities db1 = new PEPITASEntities())
            {
                DataTable dt = new DataTable();
                DateTime now = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, INDIAN_ZONE);
                CreatedDate = CreatedDate.Split('/')[2] + "-" + CreatedDate.Split('/')[1] + "-" + CreatedDate.Split('/')[0];
                HomeAPIController hm = new HomeAPIController();
                using (SqlConnection con = new SqlConnection(hm.getConnection()))
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        con.Open();
                        cmd.CommandText = @"SELECT distinct p.Id, Retailer,BatchDate, wrflow_Total AS wrflow_Total,r.region_code,p.UserId,(select employee_name From user_master us where us.user_id=p.UserId) UserName FROM Pip_MasterDetails p 
                        join region_list r on r.region_id = p.region
                        join user_master u on r.region_id = p.region
                        where wrflow_Total >= 1000 and BatchDate = '" + CreatedDate + "' and p.UserId in (case when p.UserId = 0 then '-' else u.user_id end)";
                        cmd.CommandType = CommandType.Text;
                        cmd.Connection = con;
                        cmd.Connection = con;
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        da.Fill(dt);
                        con.Close();
                    }
                }
                List<highRetailerList> list = new List<highRetailerList>();
                for (int m = 0; m < dt.Rows.Count; m++)
                {
                    highRetailerList row = new highRetailerList();
                    row.Id = Convert.ToInt32(dt.Rows[m]["Id"].ToString());
                    row.Retailer = dt.Rows[m]["Retailer"].ToString();
                    row.region_code = dt.Rows[m]["region_code"].ToString();
                    row.BatchDate = String.Format("{0:dd/MM/yyyy}", dt.Rows[m]["BatchDate"]);
                    row.wrflow_Total = Convert.ToInt32(dt.Rows[m]["wrflow_Total"].ToString()); ;
                    int userid = Convert.ToInt32(dt.Rows[m]["UserId"].ToString());
                    if (userid == 0)
                        row.agentName = "-";
                    else
                        row.agentName = dt.Rows[m]["UserName"].ToString();

                    list.Add(row);
                }
                return list;
            }
        }

        #endregion
        [HttpPost]
        #region Insert Retailer Target spilit
        [Route("api/Master/InsertTargtSpilit")]
        public string InsertTargtSpilit(TargetSpilit work)
        {
            try
            {
                using (PEPITASEntities con = new PEPITASEntities())
                {
                    DateTime now = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, INDIAN_ZONE);
                    var result = con.TargetSpilits.FirstOrDefault();
                    if (result != null)
                    {
                        result.updated_by = work.updated_by;
                        result.Target = work.Target;
                        result.Active = true;
                        result.updated_on = now;
                        conn.Entry(result).State = System.Data.Entity.EntityState.Modified;
                        conn.SaveChanges();
                    }
                }
                return "Successfully Saved !";
            }
            catch (Exception e)
            {
                return "error: " + e.Message.ToString();
            }
        }
        #endregion

        #region Get Retailer Target spilit
        [Route("api/Master/GetTargtSpilit")]
        public int GetTargtSpilit()
        {
            using (PEPITASEntities con = new PEPITASEntities())
            {
                int targetList = 0;
                targetList = Convert.ToInt32(con.TargetSpilits.FirstOrDefault().Target);
                return targetList;
            }
        }
        #endregion
        #region Get WorkFlow Batches
        [Route("api/Master/GetWorkFlowBatches")]
        public string GetWorkFlowBatches(string batchDate)
        {
            var bDate = Convert.ToDateTime(batchDate);
            bDate = bDate.Date;
            HomeAPIController hm = new HomeAPIController();
            using (SqlConnection connn = new SqlConnection(hm.getConnection()))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    connn.Open();
                    cmd.CommandText = "USP_RetailerSetAllocation";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = connn;
                    cmd.Parameters.AddWithValue("@batchdate", String.Format("{0:yyyy-MM-dd}", bDate));
                    cmd.ExecuteNonQuery();
                    connn.Close();
                }
            }
            using (PEPITASEntities con = new PEPITASEntities())
            {
                var list = con.RetailerGroupAllocations.Where(x => DbFunctions.TruncateTime(x.Batch_date) == DbFunctions.TruncateTime(bDate)).ToList();
                var priorityCount = list.Where(x => x.ISpriority == "Y" && x.SetStatus == false).Count();
                var assignedPriorityCount = list.Where(x => x.ISpriority == "Y" && x.SetStatus == true).Count();
                var normalRetailerCount = list.Where(x => x.ISpriority == "N" && x.SetStatus == false).Count();
                var assignedNormalRetailerCount = list.Where(x => x.ISpriority == "N" && x.SetStatus == true).Count();
                var result = new { priorityCount = priorityCount, assignedPriorityCount = assignedPriorityCount, normalRetailerCount = normalRetailerCount, assignedNormalRetailerCount = assignedNormalRetailerCount };
                return JsonConvert.SerializeObject(result);
            }
        }
        #endregion
        [HttpPost]
        #region Get WorkFlow Batches for udpates
        [Route("api/Master/GetWorkFlowBatchesUpdate")]
        public string GetWorkFlowBatchesUpdate(WokflowSpilit work)
        {
            var bDate = Convert.ToDateTime(work.batchDate);
            bDate = bDate.Date;
            HomeAPIController hm = new HomeAPIController();
            DateTime now = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, INDIAN_ZONE);
            using (PEPITASEntities con = new PEPITASEntities())
            {
                for (int i = 0; i < work.UserId.Count(); i++)
                {
                    var list = con.RetailerGroupAllocations.Where(x => DbFunctions.TruncateTime(x.Batch_date) == DbFunctions.TruncateTime(bDate) && x.ISpriority == work.ISpriority && x.SetStatus == false).FirstOrDefault();
                    if (list != null)
                    {
                        using (SqlConnection connn = new SqlConnection(hm.getConnection()))
                        {
                            using (SqlCommand cmd = new SqlCommand())
                            {
                                connn.Open();
                                cmd.CommandText = "Udate_WorkFlow";
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.Connection = connn;
                                cmd.Parameters.AddWithValue("@UserId", work.UserId[i]);
                                cmd.Parameters.AddWithValue("@MasterIds", list.Retailers);
                                cmd.Parameters.AddWithValue("@AllocationId", list.Id);
                                cmd.Parameters.AddWithValue("@assigned_time", now);
                                cmd.ExecuteNonQuery();
                                connn.Close();
                            }
                        }
                    }
                }
            }
            return "Successfully Saved !";
        }
        #endregion

        [Route("api/Master/GetNonAssingedUserlist")]
        public List<user_master> GetNonAssingedUserlist(string batchDate)
        {
            HomeAPIController hm = new HomeAPIController();
            DataTable dt = new DataTable();
            using (SqlConnection con = new SqlConnection(hm.getConnection()))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = @"select *from user_master where user_master.user_id not in(select UserId from  Pip_MasterDetails where BatchDate ='" + batchDate + "' and(Total = 0 or isnull(Remarks, '') = '') and status = 1) and is_delete=0";
                    cmd.CommandType = CommandType.Text;
                    cmd.Connection = con;
                    cmd.Connection = con;
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(dt);
                    con.Close();
                }
            }
            List<user_master> list = new List<user_master>();
            for (int m = 0; m < dt.Rows.Count; m++)
            {
                user_master row = new user_master();
                row.user_id = Convert.ToInt32(dt.Rows[m]["user_id"].ToString());
                row.employee_name = dt.Rows[m]["employee_name"].ToString();
                list.Add(row);
            }         
            return list;
        }
    }
}