﻿using Newtonsoft.Json;
using OfficeOpenXml;
using Pepitas_API.Entities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using Wgsn.Models;
using System.Drawing;
using System.Drawing.Imaging;
using System.Data;

namespace Pepitas_API.Controllers
{
    public class TaggedProductController : ApiController
    {
        #region Variable Declaration

        private PEPITASEntities db = new PEPITASEntities();

        #endregion Variable Declaration

        #region Tagged Products

        [Route("api/TaggedProduct/GetTaggedProducts")]
        public List<Sp_UserTagged_Products_Result> GetTaggedProducts(string FromDate = "", string ToDate = "")
        {
            var Result = db.Sp_UserTagged_Products(FromDate, ToDate, "").ToList();
            return Result;
        }

        #endregion Tagged Products

        public string getConnection()
        {
            return System.Configuration.ConfigurationManager.AppSettings["Connection"].ToString();
        }

        #region Tagged Products

        [Route("api/TaggedProduct/GetUserTaggedProducts_Withoutfillters")]
        // public List<Sp_UsertaggedExport_products_Result> GetUserTaggedProducts_Withoutfillters(string UserId="")
        public string GetUserTaggedProducts_Withoutfillters(string UserId = "")
        {
            DataTable dt = new DataTable();
            using (SqlConnection con = new SqlConnection(getConnection())) //basil
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "Sp_UsertaggedExport_products_v1";
                    cmd.Parameters.AddWithValue("@FromDate", "");
                    cmd.Parameters.AddWithValue("@To", "");
                    cmd.Parameters.AddWithValue("@UserId", UserId);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(dt);
                    con.Close();
                }
            }
            var data = (from DataRow row in dt.Rows

                        select new
                        {
                            user_id = row["user_id"].ToString(),
                            CreatedDate = row["CreatedDate"].ToString(),  //String.Format("{0:dd/MM/yyyy}", row["CreatedDate"]),
                            Analyst = row["Analyst"].ToString(),
                            NoofRetailer = row["NoofRetailer"].ToString(),
                            TaxonomyTagged = Convert.ToInt32(row["TaxonomyTagged"].ToString()),
                            EntitiesTagged = Convert.ToInt32(row["EntitiesTagged"].ToString()),
                            Outofscope = Convert.ToInt32(row["Outofscope"].ToString()),
                            TotalTaggingCount = Convert.ToInt32(row["TotalTaggingCount"].ToString()),
                            TotalOOs = Convert.ToInt32(row["TotalOOs"].ToString()),
                            Worked_Date = row["Worked_Date"].ToString() != "" ? Convert.ToDateTime(row["Worked_Date"]).ToString("yyyy-MM-dd") : ""
                        }).ToList();

            return JsonConvert.SerializeObject(data);

            //var Result = db.Sp_UsertaggedExport_products("", "", UserId).ToList();
            //return Result;
        }

        #endregion Tagged Products

        #region Tagged Products

        [Route("api/TaggedProduct/GetUserTaggedProducts")]
        public string GetUserTaggedProducts(string UserId, string FromDate, string ToDate)
        {
            if (UserId == null) UserId = "";
            DateTime From = DateTime.ParseExact(FromDate.Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
            DateTime To = DateTime.ParseExact(ToDate.Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
            //var Result = db.Sp_UsertaggedExport_products(String.Format("{0:yyyy-MM-dd}", From), String.Format("{0:yyyy-MM-dd}", To), UserId).ToList();
            //return Result;

            DataTable dt = new DataTable();
            using (SqlConnection con = new SqlConnection(getConnection())) //basil
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandText = "Sp_UsertaggedExport_products_v1";
                    cmd.Parameters.AddWithValue("@FromDate", String.Format("{0:yyyy-MM-dd}", From));
                    cmd.Parameters.AddWithValue("@To", String.Format("{0:yyyy-MM-dd}", To));
                    cmd.Parameters.AddWithValue("@UserId", UserId);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(dt);
                    con.Close();
                }
            }
            var data = (from DataRow row in dt.Rows

                        select new
                        {
                            user_id = row["user_id"].ToString(),
                            CreatedDate = row["CreatedDate"].ToString(),  //String.Format("{0:dd/MM/yyyy}", row["CreatedDate"]),
                            Analyst = row["Analyst"].ToString(),
                            NoofRetailer = row["NoofRetailer"].ToString(),
                            TaxonomyTagged = Convert.ToInt32(row["TaxonomyTagged"].ToString()),
                            EntitiesTagged = Convert.ToInt32(row["EntitiesTagged"].ToString()),
                            Outofscope = Convert.ToInt32(row["Outofscope"].ToString()),
                            TotalTaggingCount = Convert.ToInt32(row["TotalTaggingCount"].ToString()),
                            TotalOOs = Convert.ToInt32(row["TotalOOs"].ToString()),
                            Worked_Date = row["Worked_Date"].ToString() != "" ? Convert.ToDateTime(row["Worked_Date"]).ToString("yyyy-MM-dd") : ""
                        }).ToList();

            return JsonConvert.SerializeObject(data);
        }

        #endregion Tagged Products

        #region User TaggedProducts Export Results

        ////[Route("api/TaggedProduct/UserTaggedProducts/Export/{FromDate}/{ToDate}/{UserId?}")]
        //[HttpGet]
        //[Route("api/TaggedProduct/UserTaggedProducts/Export")]
        //public void UserTaggedProducts(string FromDate, string ToDate, string UserId = "")
        //{
        //    using (WgsnEntities db = new WgsnEntities())
        //    {
        //        if (!string.IsNullOrEmpty(UserId))
        //            UserId = UserId.Replace("-", ",");
        //        DateTime From = DateTime.ParseExact(FromDate.Trim(), "dd-MM-yyyy", CultureInfo.InvariantCulture);
        //        DateTime To = DateTime.ParseExact(ToDate.Trim(), "dd-MM-yyyy", CultureInfo.InvariantCulture);
        //        string currentdate = From.ToString("MM/dd/yyyy") + " " + To.ToString("MM/dd/yyyy") + "";
        //        var data = db.Sp_UsertaggedExport_products(String.Format("{0:yyyy-MM-dd}", From), String.Format("{0:yyyy-MM-dd}", To), UserId).ToList();
        //        var Detailed_data = db.Sp_UsertaggedExport_products_Details(String.Format("{0:yyyy-MM-dd}", From), String.Format("{0:yyyy-MM-dd}", To), Convert.ToInt32(UserId)).ToList();

        //        HttpContext.Current.Response.Clear();
        //        HttpContext.Current.Response.ClearContent();
        //        HttpContext.Current.Response.ClearHeaders();
        //        HttpContext.Current.Response.Buffer = true;
        //        HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.UTF8;
        //        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        //        HttpContext.Current.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        //        HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=" + currentdate + ".xlsx");
        //        //CommonMethod mthod = new CommonMethod();
        //        //var dt = mthod.ToDataTable(data);
        //        using (ExcelPackage pack = new ExcelPackage())
        //        {
        //            ExcelWorksheet ws = pack.Workbook.Worksheets.Add("Results");
        //            ws.Cells["A1"].LoadFromCollection(data, true);
        //            ws.Cells["A1"].Value = "Date";
        //            ws.Cells["B1"].Value = "Analyst";
        //            ws.Cells["C1"].Value = "No of Retailer";
        //            ws.Cells["D1"].Value = "Taxonomy Tagged";
        //            ws.Cells["E1"].Value = "Entities Tagged";
        //            ws.Cells["F1"].Value = "Out of scope";
        //            ws.Cells["G1"].Value = "Total Tagging Count";
        //            ws.Cells["H1"].Value = "Total+OOs";
        //            ws = pack.Workbook.Worksheets.Add("Detailed_Report");
        //            ws.Cells["A1"].LoadFromCollection(Detailed_data, true);
        //            var ms = new System.IO.MemoryStream();
        //            pack.SaveAs(ms);
        //            ms.WriteTo(HttpContext.Current.Response.OutputStream);
        //        }

        //        HttpContext.Current.Response.Flush();
        //        HttpContext.Current.Response.End();

        //    }
        //}

        [HttpGet]
        [Route("api/TaggedProduct/UserTaggedProducts/Export")]
        public string UserTaggedProducts(string FromDate, string ToDate, string UserId = "")
        {
            using (PEPITASEntities db = new PEPITASEntities())
            {
                if (!string.IsNullOrEmpty(UserId))
                    UserId = UserId.Replace("-", ",");
                else
                    UserId = "0";
                DateTime From = DateTime.ParseExact(FromDate.Trim(), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                DateTime To = DateTime.ParseExact(ToDate.Trim(), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                string currentdate = From.ToString("MM/dd/yyyy") + " " + To.ToString("MM/dd/yyyy") + "";
                //var data = db.Sp_UsertaggedExport_products(String.Format("{0:yyyy-MM-dd}", From), String.Format("{0:yyyy-MM-dd}", To), UserId).ToList();
                var Detailed_data = db.Sp_UsertaggedExport_products_Details(String.Format("{0:yyyy-MM-dd}", From), String.Format("{0:yyyy-MM-dd}", To), UserId).ToList();
                DataTable dt = new DataTable();
                using (SqlConnection con = new SqlConnection(getConnection())) //basil
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        con.Open();
                        cmd.CommandText = "Sp_UsertaggedExport_products_v1";
                        cmd.Parameters.AddWithValue("@FromDate", String.Format("{0:yyyy-MM-dd}", From));
                        cmd.Parameters.AddWithValue("@To", String.Format("{0:yyyy-MM-dd}", To));
                        cmd.Parameters.AddWithValue("@UserId", UserId);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Connection = con;
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        da.Fill(dt);
                        con.Close();
                    }
                }

                var data = (from DataRow row in dt.Rows
                            select new
                            {
                                user_id = row["user_id"].ToString(),
                                CreatedDate = row["CreatedDate"].ToString(),  //String.Format("{0:dd/MM/yyyy}", row["CreatedDate"]),
                                Analyst = row["Analyst"].ToString(),
                                NoofRetailer = row["NoofRetailer"].ToString(),
                                TaxonomyTagged = Convert.ToInt32(row["TaxonomyTagged"].ToString()),
                                EntitiesTagged = Convert.ToInt32(row["EntitiesTagged"].ToString()),
                                Outofscope = Convert.ToInt32(row["Outofscope"].ToString()),
                                TotalTaggingCount = Convert.ToInt32(row["TotalTaggingCount"].ToString()),
                                TotalOOs = Convert.ToInt32(row["TotalOOs"].ToString()),
                                //Worked_Date = row["Worked_Date"].ToString() != "" ? Convert.ToDateTime(row["Worked_Date"]).ToString("yyyy-MM-dd") : ""
                            }).ToList();
                var result = new { Result1 = data, Result2 = Detailed_data };
                return JsonConvert.SerializeObject(result);
            }
        }

        #endregion User TaggedProducts Export Results

        #region Get Users

        [Route("api/TaggedProduct/GetUserlist")]
        public List<user_master> GetUserlist()
        {
            var Userlist = db.user_master.Where(x => x.is_delete == false).OrderBy(Y => Y.user_id).ToList();
            return Userlist;
        }

        #endregion Get Users

        #region Region Inflow

        [HttpGet]
        [Route("api/TaggedProduct/GetRegionlist")]
        public List<MRG_Regionlist_Result> GetRegionlist()
        {
            return db.MRG_Regionlist(1).ToList();
        }

        [HttpGet]
        [Route("api/TaggedProduct/GetRegioninflow_list")]
        public List<MRG_Attribute_inflow_Result> GetRegioninflow_list(string RegionId, string Attribute_value, string FromDate, string ToDate)
        {
            FromDate = FromDate.Split('/')[1] + "/" + FromDate.Split('/')[0] + "/" + FromDate.Split('/')[2];
            ToDate = ToDate.Split('/')[1] + "/" + ToDate.Split('/')[0] + "/" + ToDate.Split('/')[2];
            var list = db.MRG_Attribute_inflow(RegionId.Trim(), Attribute_value, FromDate, ToDate).ToList();
            return list;
        }

        //[Route("api/TaggedProduct/RegionInflow/Export/{RegionId}/{FromDate}/{ToDate}")]
        //public void RegionInflowExport(string RegionId, string FromDate, string ToDate)
        //{
        //    using (WgsnEntities db = new WgsnEntities())
        //    {
        //        List<Fetch_Attribute_Inflow_Result> data = db.Fetch_Attribute_Inflow(RegionId, DateTime.ParseExact(FromDate.Trim(), "dd-MM-yyyy", CultureInfo.InvariantCulture), DateTime.ParseExact(ToDate.Trim(),
        //            "dd-MM-yyyy", CultureInfo.InvariantCulture)).ToList();
        //        HttpContext.Current.Response.Clear();
        //        HttpContext.Current.Response.ClearContent();
        //        HttpContext.Current.Response.ClearHeaders();
        //        HttpContext.Current.Response.Buffer = true;
        //        HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.UTF8;
        //        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        //        HttpContext.Current.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        //        HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=" + FromDate + " To " + ToDate + " Inflow.xlsx");

        //        using (ExcelPackage pack = new ExcelPackage())
        //        {
        //            ExcelWorksheet ws = pack.Workbook.Worksheets.Add("Results");
        //            ws.Cells["A1"].LoadFromCollection(data, true);
        //            var ms = new System.IO.MemoryStream();
        //            pack.SaveAs(ms);
        //            ms.WriteTo(HttpContext.Current.Response.OutputStream);
        //        }
        //        HttpContext.Current.Response.Flush();
        //        HttpContext.Current.Response.End();
        //    }
        //}

        [HttpGet]
        [Route("api/TaggedProduct/RegionInflow/Export/{RegionId}/{FromDate}/{ToDate}")]
        public List<Fetch_Attribute_Inflow_Result> RegionInflowExport(string RegionId, string FromDate, string ToDate)
        {
            using (PEPITASEntities db = new PEPITASEntities())
            {
                List<Fetch_Attribute_Inflow_Result> data = db.Fetch_Attribute_Inflow(RegionId, DateTime.ParseExact(FromDate.Trim(), "dd-MM-yyyy", CultureInfo.InvariantCulture), DateTime.ParseExact(ToDate.Trim(),
                    "dd-MM-yyyy", CultureInfo.InvariantCulture)).ToList();

                return data;
            }
        }

        [HttpGet]
        [Route("api/TaggedProduct/RegionWiseCount")]
        public string Region_wise_count(string CurrentDate)
        {

            try
            {
                DataTable ds = new DataTable();
                
                using (SqlConnection con = new SqlConnection(getConnection())) //basil
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        con.Open();
                        cmd.CommandText = " select region_code as Region,sum(weightage) as CurrentBatchDate from dbo.Pip_MasterDetails p  with (nolock) join dbo.region_list r  with(nolock) on r.region_id = p.region where p.BatchDate = '" + CurrentDate + "'group by region_code";
                        cmd.CommandType = CommandType.Text;
                        cmd.Connection = con;
                        //cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Connection = con;
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        da.Fill(ds);
                        con.Close();
                    }
                }
                return JsonConvert.SerializeObject(ds);
            }
            catch (Exception e)
            {
                //return Json(new { Success = "true", Message = e.Message }, JsonRequestBehavior.AllowGet);
                return "Error :" + e.Message;
            }

            //using (PEPITASEntities db = new PEPITASEntities())
            //{
            //    //List<Region_Wise_count_Result> list = db.Region_Wise_count(CurrentDate).ToList();
            //    //return Json(list, JsonRequestBehavior.AllowGet);
            //    var list = db.Region_Wise_count(CurrentDate).ToList();
            //    return JsonConvert.SerializeObject(list);
            //}
        }

        [HttpGet]
        [Route("api/TaggedProduct/RegionWiseInsertCount")]
        public string Region_wise_insert_count(string CurrentDate, int UserId)
        {
            using (PEPITASEntities db = new PEPITASEntities())
            {
                var list = db.Region_wise_insert_count(CurrentDate, UserId).ToList();
                //return Json(list, JsonRequestBehavior.AllowGet);
                return JsonConvert.SerializeObject(list);
            }
        }

        #endregion Region Inflow

        #region Dashboard

        [Route]
        [Route("api/TaggedProduct/GetHomeRegionlist/{CurrentDate}/{PreviousDate}")]
        public List<HomeRegion> GetHomeRegionlist(string CurrentDate, string PreviousDate)
        {
            using (PEPITASEntities db = new PEPITASEntities())
            {
                List<HomeRegion> HomeRegion = db.Database.SqlQuery<HomeRegion>("Home_Page_Region_Wise_Report @CurrentBatchDate, @PreviousBatchDate",
                    new SqlParameter("CurrentBatchDate", CurrentDate), new SqlParameter("PreviousBatchDate", PreviousDate)).ToList();
                return HomeRegion;
            }
        }

        [HttpGet]
        [Route("api/TaggedProduct/GetAttributeInflow/{CurrentDate}/{PreviousDate}")]
        public List<Get_Attribute_Inflow_Result> GetAttributeInflow(string CurrentDate, string PreviousDate)
        {
            var Attributelist = db.Get_Attribute_Inflow(DateTime.ParseExact(CurrentDate.Trim(), "yyyy-MM-dd", CultureInfo.InvariantCulture), DateTime.ParseExact(PreviousDate.Trim(), "yyyy-MM-dd", CultureInfo.InvariantCulture)).ToList();
            return Attributelist;
        }

        #endregion Dashboard

        #region screenshot

        [HttpGet]
        [Route("api/TaggedProduct/GetScreenshotGrid")]
        public string screenshot_grid(int UserId, string date)
        {
            using (PEPITASEntities db = new PEPITASEntities())
            {
                try
                {
                    var grid = db.USP_Get_screenshot(UserId).ToList();
                    if (!string.IsNullOrEmpty(date))
                    {
                        grid = grid.Where(c => c.batch_date == date).ToList();
                    }

                    return JsonConvert.SerializeObject(grid);
                }
                catch (Exception e)
                {
                    return "error: " + e.ToString();
                }
            }
        }

        //[HttpPost]
        //[Route("api/TaggedProduct/UploadFile")]
        //public string upload_file(int regionId, int userId, string batchDate, bool isActive, string originalFileName)
        //public string upload_file(ScreenShot obj)
        //{
        //    try
        //    {
        //        //string ID = Request.Form[0].ToString();//Session["UserId"].ToString();
        //        //string region = Request.Form[1].ToString();
        //        //DateTime batch_date = Convert.ToDateTime(Request.Form[2].ToString());
        //        //string is_activedata = Request.Form[3].ToString();
        //        //bool is_active = (is_activedata == "1") ? true : false; ;
        //        //var path = AppDomain.CurrentDomain.BaseDirectory + "ScreenShots" + "\\" + DateTime.Now.ToString("ddMMyyyy") + "\\";
        //        //var ret_path = "ScreenShots" + "\\" + DateTime.Now.ToString("ddMMyyyy") + "\\";

        //        //var Upload_file = Request.Files[0];

        //        //string fileName = "";
        //        //if (!Directory.Exists(path))
        //        //{
        //        //    DirectoryInfo di = Directory.CreateDirectory(path);
        //        //}
        //        //if (Upload_file != null && Upload_file.ContentLength > 0)
        //        //{
        //        //    fileName = ID + "_" + region + "_" + DateTime.Now.ToString("ddMMyyyy") + "_" + Path.GetFileName(Upload_file.FileName);
        //        //    ret_path = ret_path + fileName;
        //        //    path = path + ID + "_" + region + "_" + DateTime.Now.ToString("ddMMyyyy") + "_R_" + Path.GetFileName(Upload_file.FileName);

        //        //    Upload_file.SaveAs(path);
        //        //    var pathdts = "ScreenShots" + "\\" + DateTime.Now.ToString("ddMMyyyy") + "\\" + fileName;
        //        //    string filesaved = AppDomain.CurrentDomain.BaseDirectory + pathdts;
        //        //    //First load the image somehow
        //        //    Image myImage = Image.FromFile(path, true);
        //        //    //// Save the image with a quality of 50%
        //        //    SaveJpeg(filesaved, myImage, 50);
        //        //    myImage.Dispose();
        //        //    using (PEPITASEntities db = new PEPITASEntities())
        //        //    {
        //        //        db.USP_INSERT_SCREENSHOT(Convert.ToInt32(region), Convert.ToInt32(ID), pathdts, batch_date, is_active);
        //        //    }

        //        //    if (System.IO.File.Exists(path))
        //        //    {
        //        //        System.IO.File.Delete(path);
        //        //    }

        //        //}

        //        string fileName = obj.userId + "_" + obj.regionId + "_" + DateTime.Now.ToString("ddMMyyyy") + "_" + obj.originalFileName;
        //        var pathdts = "ScreenShots" + "\\" + DateTime.Now.ToString("ddMMyyyy") + "\\" + fileName;

        //        var ret_path = "ScreenShots" + "\\" + DateTime.Now.ToString("ddMMyyyy") + "\\";
        //        ret_path = ret_path + fileName;

        //        DateTime batch_date = Convert.ToDateTime(obj.batchDate);
        //        using (WgsnEntities db = new WgsnEntities())
        //        {
        //            db.USP_INSERT_SCREENSHOT(obj.regionId, obj.userId, pathdts, batch_date, obj.isActive);
        //        }

        //        return "success";
        //    }
        //    catch (Exception ex)
        //    {
        //        return ex.Message;
        //    }

        //}

        [HttpPost]
        [Route("api/TaggedProduct/UploadScreenShot")]
        public HttpResponseMessage UploadScreenShot()
        {
            HttpResponseMessage response = new HttpResponseMessage();
            var path = AppDomain.CurrentDomain.BaseDirectory + "ScreenShots";
            if (!System.IO.Directory.Exists(path))
            {
                System.IO.DirectoryInfo di = System.IO.Directory.CreateDirectory(path);
            }

            var httpRequest = HttpContext.Current.Request;
            if (httpRequest.Files.Count > 0)
            {
                foreach (string file in httpRequest.Files)
                {
                    //var postedFile = httpRequest.Files[0];
                    var postedFile = httpRequest.Files[file];
                    string fileName = postedFile.FileName;
                    path = path + "\\" + fileName;
                    if (System.IO.File.Exists(path))
                    {
                        System.IO.File.Delete(path);
                    }
                    var filePath = HttpContext.Current.Server.MapPath("~/ScreenShots/" + postedFile.FileName);
                    postedFile.SaveAs(filePath);
                }
            }

            if (httpRequest.Form.Count > 0)
            {
                foreach (string form in httpRequest.Form)
                {
                    //var postedForm = httpRequest.Form["body"];
                    var postedForm = httpRequest.Form[form];
                    dynamic json = JsonConvert.DeserializeObject(postedForm);

                    var pathdts = "ScreenShots" + "\\" + DateTime.Now.ToString("ddMMyyyy") + "\\" + json.fileName;
                    int regionId = json.regionId;
                    int userId = json.userId;
                    string batchDate = json.batchDate;
                    bool isActive = json.isActive;
                    string fileName = json.fileName;

                    DateTime batch_date = Convert.ToDateTime(batchDate.ToString());
                    using (PEPITASEntities db = new PEPITASEntities())
                    {
                        db.USP_INSERT_SCREENSHOT(regionId, userId, pathdts, batch_date, isActive);
                    }
                }
            }

            return response;
        }

        [HttpPost]
        [Route("api/TaggedProduct/DownloadScreenShot")]
        public HttpResponseMessage DownloadScreenShot(string fileName)
        {
            System.IO.MemoryStream ms = null;
            HttpContext context = HttpContext.Current;
            //Limit access only to images folder at root level
            string filePath = HttpContext.Current.Server.MapPath("~/ScreenShots/" + fileName);
            string extension = System.IO.Path.GetExtension(fileName);
            if (System.IO.File.Exists(filePath))
            {
                if (!string.IsNullOrWhiteSpace(extension))
                {
                    extension = extension.Substring(extension.IndexOf(".") + 1);
                }

                //If requested file is an image than load file to memory
                if (GetImageFormat(extension) != null)
                {
                    ms = CopyFileToMemory(filePath);
                }
            }

            if (ms == null)
            {
                extension = "png";
                ms = CopyFileToMemory(HttpContext.Current.Server.MapPath("~/ScreenShots/Image-not-found.png"));
            }

            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
            result.Content = new ByteArrayContent(ms.ToArray());
            result.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue(string.Format("image/{0}", extension));
            return result;
        }

        public static ImageFormat GetImageFormat(string extension)
        {
            ImageFormat result = null;
            System.Reflection.PropertyInfo prop = typeof(ImageFormat).GetProperties().Where(p => p.Name.Equals(extension, StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault();
            if (prop != null)
            {
                result = prop.GetValue(prop) as ImageFormat;
            }
            return result;
        }

        private System.IO.MemoryStream CopyFileToMemory(string path)
        {
            System.IO.MemoryStream ms = new System.IO.MemoryStream();
            System.IO.FileStream fs = new System.IO.FileStream(path, System.IO.FileMode.Open);
            fs.Position = 0;
            fs.CopyTo(ms);
            fs.Close();
            fs.Dispose();
            return ms;
        }

        [HttpDelete]
        [Route("api/TaggedProduct/DeleteScreenshot")]
        public string screenshot_delete(int id, string fileName)
        {
            using (PEPITASEntities db = new PEPITASEntities())
            {
                try
                {
                    var path = AppDomain.CurrentDomain.BaseDirectory + "ScreenShots";
                    path = path + "\\" + fileName;
                    if (System.IO.File.Exists(path))
                    {
                        System.IO.File.Delete(path);
                    }

                    var delete = db.screen_shot.FirstOrDefault(s => s.Id == id);
                    if (delete != null)
                    {
                        db.screen_shot.Remove(delete);
                        db.SaveChanges();
                    }
                    return "success";
                }
                catch (Exception e)
                {
                    return "error: " + e.ToString();
                }
            }
        }

        #endregion screenshot
    }
}