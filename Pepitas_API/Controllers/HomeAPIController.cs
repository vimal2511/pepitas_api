﻿using Newtonsoft.Json;
using OfficeOpenXml;
using Pepitas_API.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Web;
using System.Web.Http;
using Wgsn.Models;

namespace Pepitas_API.Controllers
{
    public class HomeAPIController : ApiController
    {
        #region Variable Declaration
        PEPITASEntities db = new PEPITASEntities();
        public TimeZoneInfo INDIAN_ZONE = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");
        #endregion

        #region  Pepitas Entry

        [Route("api/HomeAPI/GetAgentName")]
        public List<user_master> GetAgentName()
        {
            List<user_master> result = db.user_master.Where(x => x.role_id == 3 && x.is_active == true && x.is_delete == false).ToList();
            return result;
        }
        [HttpGet]
        [Route("api/HomeAPI/Test/{Text}")]
        public string TestMethod(string Text)
        {
            return Text;
        }

        [HttpPost]
        [Route("api/HomeAPI/CalculateBreak")]
        public string Calculate_Break(BreakTime obj)
        {

            try
            {
                using (PEPITASEntities db = new PEPITASEntities())
                {
                    db.USP_Insert_Break(obj.Master_ID.ToString(), obj.Break_start.ToString(), obj.Start_Time.ToString(), obj.diff_time);
                }
                return "success";
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        [HttpPost]
        [Route("api/HomeAPI/SaveMasterDetails")]
        public string SaveMasterDetails(List<Pip_MasterDetails> pip)
        {

            //try
            //{
            //    for (int i = 0; i < pip.Count(); i++)
            //    {
            //        pip[i].Deleted = false;
            //        if (pip[i].Id == 0)
            //            db.Pip_MasterDetails.Add(pip[i]);
            //        else
            //            db.Entry(pip[i]).State = System.Data.Entity.EntityState.Modified;
            //        db.SaveChanges();
            //    }
            //    return "Pepitas Details Added Successfully";
            //}

            //catch (Exception e)
            //{
            //    return "Error";
            //}

            using (PEPITASEntities db = new PEPITASEntities())
            {
                try
                {

                    DataTable Pip_MasterDetails_dt = new DataTable();
                    Pip_MasterDetails_dt.Columns.Add("Id", typeof(Int32));
                    Pip_MasterDetails_dt.Columns.Add("CreatedDate", typeof(DateTime));
                    Pip_MasterDetails_dt.Columns.Add("Retailer", typeof(string));
                    Pip_MasterDetails_dt.Columns.Add("Market_Dept", typeof(int));
                    Pip_MasterDetails_dt.Columns.Add("Category", typeof(int));
                    Pip_MasterDetails_dt.Columns.Add("SubCategory", typeof(Int32));
                    Pip_MasterDetails_dt.Columns.Add("Taxonomy", typeof(Int32));
                    Pip_MasterDetails_dt.Columns.Add("Brands", typeof(Int32));
                    Pip_MasterDetails_dt.Columns.Add("Materials", typeof(Int32));
                    Pip_MasterDetails_dt.Columns.Add("P_G", typeof(Int32));
                    Pip_MasterDetails_dt.Columns.Add("Colors", typeof(Int32));
                    Pip_MasterDetails_dt.Columns.Add("Sizes", typeof(Int32));
                    Pip_MasterDetails_dt.Columns.Add("OOS", typeof(Int32));
                    Pip_MasterDetails_dt.Columns.Add("Total", typeof(Int32));
                    Pip_MasterDetails_dt.Columns.Add("StartTime", typeof(string));
                    Pip_MasterDetails_dt.Columns.Add("EndTime", typeof(string));
                    Pip_MasterDetails_dt.Columns.Add("Duriation", typeof(string));
                    Pip_MasterDetails_dt.Columns.Add("Analyst", typeof(string));
                    Pip_MasterDetails_dt.Columns.Add("Remarks", typeof(string));
                    Pip_MasterDetails_dt.Columns.Add("BatchDate", typeof(DateTime));
                    Pip_MasterDetails_dt.Columns.Add("UserId", typeof(int));
                    Pip_MasterDetails_dt.Columns.Add("Deleted", typeof(bool));
                    Pip_MasterDetails_dt.Columns.Add("region", typeof(int));
                    Pip_MasterDetails_dt.Columns.Add("filename", typeof(string));
                    Pip_MasterDetails_dt.Columns.Add("status", typeof(int));
                    IFormatProvider culture = new CultureInfo("en-US", true);
                    string duration = "";
                    for (int i = 0; i < pip.Count(); i++)
                    {
                        pip[i].Deleted = false;
                        duration = pip[i].Duriation;
                        if (i == pip.Count() - 1)
                        {
                            string Master_ID = pip[i].Id.ToString();
                            if (Master_ID != "0")
                            {
                                var list = db.BreakTimes.Where(a => a.Master_ID == Master_ID).Sum(x => x.diff_time).ToString();
                                TimeSpan result = TimeSpan.FromMinutes(Convert.ToInt32(list));
                                duration = result.ToString("hh':'mm");
                                pip[i].EndTime = get_currenttime();
                                string id = Convert.ToString(pip[i].Id);
                                var starttime = db.BreakTimes.Where(x => x.Master_ID == id).OrderBy(x => x.Id).Select(x => x.Start_Time).FirstOrDefault();
                                if (starttime != null && starttime != "")
                                    pip[i].StartTime = starttime;
                            }
                        }
                      //  var batchdate = Convert.ToString(pip[i].BatchDate).Split(' ')[0].Split('-');
                      // var batchdate1 = batchdate[2] + "-" + batchdate[0] + "-" + batchdate[1];
                       // pip[i].BatchDate =Convert.ToDateTime(batchdate1);
                        //DateTime dateVal = DateTime.ParseExact(batchdate1, "yyyy-MM-dd", culture);

                        Pip_MasterDetails_dt.Rows.Add(new
                           object[] { pip[i].Id, pip[i].CreatedDate, pip[i].Retailer, pip[i].Market_Dept, pip[i].Category, pip[i].SubCategory, pip[i].Taxonomy,
                                    pip[i].Brands,pip[i].Materials,pip[i].P_G,pip[i].Colors,pip[i].Sizes,pip[i].OOS,pip[i].Total,pip[i].StartTime,pip[i].EndTime,duration,
                                    pip[i].Analyst,pip[i].Remarks,pip[i].BatchDate,pip[i].UserId,pip[i].Deleted,pip[i].region,pip[i].filename,pip[i].status
                         });
                    }
                    excuteprocedure("Usp__insert_update_pip_masterdetails", Pip_MasterDetails_dt);
                }
                catch (Exception e)
                {
                    return "Error :" + e.Message;
                }
            }
            return "Pepitas Details Added Successfully";
        }

        public string getConnection()
        {
            return ConfigurationManager.AppSettings["Connection"].ToString();
        }
        public void excuteprocedure(string procedurename, DataTable dt)
        {
            try
            {
                String strConnString = ConfigurationManager.AppSettings["connection"].ToString();
                SqlConnection con = new SqlConnection(strConnString);
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Pip_MasterDetails_list", dt);
                cmd.CommandText = procedurename;
                cmd.Connection = con;
                con.Open();
                cmd.ExecuteReader();
                con.Close();
            }
            catch (Exception ex)
            {
                var error = "Error :" + ex.Message;
            }
        }

        public string get_currenttime()
        {
            DateTime now = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, INDIAN_ZONE);
            string hr = Convert.ToInt32(now.Hour) < 10 ? "0" + now.Hour.ToString() : now.Hour.ToString();
            string mints = Convert.ToInt32(now.Minute) < 10 ? "0" + now.Minute.ToString() : now.Minute.ToString();
            return hr + ":" + mints;
        }
        [HttpGet]
        [Route("api/HomeAPI/GetAgentMapping")] //new
        public string GetAgentMapping()
        {
            using (PEPITASEntities db = new PEPITASEntities())
            {
                try
                {
                    var result = new DataTable();
                    var cmd = db.Database.Connection.CreateCommand();
                    cmd.CommandText = "USP_AgentCountryAllocation";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection.Open();
                    result.Load(cmd.ExecuteReader());
                    cmd.Connection.Close();

                    return JsonConvert.SerializeObject(result);
                }
                catch (Exception e)
                {
                    return JsonConvert.SerializeObject("error: " + e.Message.ToString());
                }
            }
        }

        [HttpGet]
        [Route("api/HomeAPI/GetMappedRecord")]
        public string GetMappedRecord()
        {
            using (PEPITASEntities db = new PEPITASEntities())
            {
                try
                {
                    var createddate = db.Mapping_Data.OrderByDescending(a => a.Created_date).FirstOrDefault().Created_date;
                    string dt = Convert.ToDateTime(createddate).ToString("yyyy-MM-dd");

                    var result = (from MD in db.Mapping_Data
                                  join RL in db.region_list on MD.region_id equals RL.region_id
                                  join UM in db.user_master on MD.agent_ids equals UM.user_id.ToString()
                                  where MD.Created_date.ToString() == dt
                                  orderby MD.region_id
                                  select new { region_id = MD.region_id, region_name = RL.region_name, user_name = UM.user_name }).ToList();


                    return JsonConvert.SerializeObject(result);
                }
                catch (Exception e)
                {
                    return JsonConvert.SerializeObject("error: " + e.Message.ToString());
                }
            }
        }

        [HttpPost]
        [Route("api/HomeAPI/CheckTicketDetails")]
        public string CheckTicketDetails(List<Pip_Ticket_details> obj)
        {
            using (PEPITASEntities db = new PEPITASEntities())
            {
                List<Pip_Ticket_details> list = new List<Pip_Ticket_details>();
                try
                {
                    for (int m = 0; m < obj.Count; m++)
                    {
                        string retailer = obj[m].Retailer.ToString();
                        Pip_Ticket_details data = db.Pip_Ticket_details.Where(x => x.Retailer == retailer && x.is_active == true).SingleOrDefault();
                        if (data != null)
                            list.Add(data);
                    }
                    return JsonConvert.SerializeObject(list);
                }
                catch (Exception e)
                {
                    return JsonConvert.SerializeObject("error: " + e.Message.ToString());
                }
            }
        }
        public class updateagent
        {
            public int Id { get; set; }
            public int agentid { get; set; }
        }
        [HttpPost]
        [Route("api/HomeAPI/Updateagentretailer")]
        public string Updateagentretailer(List<updateagent> obj)
        {
            using (PEPITASEntities db = new PEPITASEntities())
            {
                try
                {
                    
                    for (int m = 0; m < obj.Count; m++)
                    {
                        int agentid = obj[m].agentid;
                        int rowid = obj[m].Id;
                        DateTime now = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, INDIAN_ZONE);
                        Pip_MasterDetails list = new Pip_MasterDetails();

                       list = db.Pip_MasterDetails.Where(x => x.Id == rowid).SingleOrDefault();
                        list.UserId = agentid;
                        list.assigned_time = now;
                        list.Market_Dept = 0;
                        list.Category = 0;
                        list.SubCategory = 0;
                        list.Taxonomy = 0;
                        list.Brands = 0;
                        list.Materials = 0;
                        list.OOS = 0;
                        list.P_G = 0;
                        list.Colors = 0;
                        list.Sizes = 0;
                        list.Total = 0;
                        list.Remarks = "";
                        list.StartTime = "00:00";
                        list.EndTime = "00:00";
                        list.Duriation = "00:00";
                        list.status = 1;
                        db.Pip_MasterDetails.Add(list);
                       db.SaveChanges();
                    }
                    return JsonConvert.SerializeObject("successfully added");
                }
                catch (Exception e)
                {
                    return JsonConvert.SerializeObject("error: " + e.Message.ToString());
                }
            }
        }


        //[HttpPost]
        //[Route("api/HomeAPI/SaveMasterDetails")]
        //public string SaveMasterDetails(Pip_MasterDetails pip)
        //{

        //    try
        //    {
        //        if (pip != null)
        //        {
        //            pip.Deleted = false;
        //            if (pip.Id == 0)
        //                db.Pip_MasterDetails.Add(pip);
        //            else
        //                db.Entry(pip).State = System.Data.Entity.EntityState.Modified;
        //            db.SaveChanges();
        //        }
        //        return "Pepitas Details Added Successfully";
        //    }

        //    catch (Exception e)
        //    {
        //        return "Error";
        //    }           
        //}

        [Route("api/HomeAPI/GetMasterdetails")]
        public string GetMasterdetails(string CreatedDate, int RoleId, int UserId, int? region_id = 0)
        {

            //CreatedDate = CreatedDate.Split('/')[1] + "/" + CreatedDate.Split('/')[0] + "/" + CreatedDate.Split('/')[2];
            ////DateTime CreateDate = Convert.ToDateTime(CreatedDate);
            //List<Sp_getmaster_details_Result> Pip_MasterDetail = new List<Sp_getmaster_details_Result>();            
            //if (RoleId == 3)
            //    Pip_MasterDetail = db.Sp_getmaster_details(CreatedDate, UserId).ToList();                
            //else
            //    Pip_MasterDetail = db.Sp_getmaster_details(CreatedDate, 0).ToList();               
            //var list = Pip_MasterDetail.Select(u => new
            //{
            //    Id = u.Id,
            //    BatchDate = String.Format("{0:dd/MM/yyyy}", u.BatchDate),               
            //    Retailer = u.Retailer,
            //    regionlist = db.MRG_Regionlist(1).ToList(),
            //    Region = u.region.ToString(),
            //    Market_Dept = u.Market_Dept,
            //    CreatedDate = u.CreatedDate,
            //    P_G = u.P_G,
            //    Colors = u.Colors,
            //    Sizes = u.Sizes,
            //    Category = u.Category,
            //    SubCategory = u.SubCategory,
            //    Taxonomy = u.Taxonomy,
            //    Brands = u.Brands,
            //    Materials = u.Materials,
            //    OOS = u.OOS,
            //    Total = u.Total,
            //    StartTime = u.StartTime,
            //    EndTime = u.EndTime,
            //    Duriation = u.Duriation,
            //    Analyst = u.Analyst,
            //    Remarks = u.Remarks,
            //    UserId = u.UserId
            //}).ToList();

            //return JsonConvert.SerializeObject(list);

            using (PEPITASEntities db = new PEPITASEntities())
            {
                try
                {
                    //CreatedDate = CreatedDate.Split('/')[0] + "/" + CreatedDate.Split('/')[1] + "/" + CreatedDate.Split('/')[2];
                    CreatedDate = CreatedDate.Split('/')[2] + "-" + CreatedDate.Split('/')[1] + "-" + CreatedDate.Split('/')[0];
                    //DateTime CreateDate = Convert.ToDateTime(CreatedDate);

                    DataSet ds = new DataSet();
                    if (RoleId != 3)
                        UserId = 0;
                    using (SqlConnection con = new SqlConnection(getConnection())) //basil
                    {
                        using (SqlCommand cmd = new SqlCommand())
                        {
                            con.Open();
                            //cmd.CommandText = "usp_getretailer";
                            cmd.CommandText = "usp_retailerallocation";
                            cmd.Parameters.AddWithValue("@CreatedDate", CreatedDate);
                            cmd.Parameters.AddWithValue("@userid", UserId);
                            cmd.Parameters.AddWithValue("@region_Id", region_id);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Connection = con;
                            SqlDataAdapter da = new SqlDataAdapter(cmd);
                            da.Fill(ds);
                            con.Close();
                        }
                    }

                    for (int m = 0; m < ds.Tables[0].Rows.Count; m++)
                    {
                        if (ds.Tables[0].Rows[m]["status"].ToString() == "1")
                        {
                            ds.Tables[0].Rows[m]["StartTime"] = get_currenttime();
                        }
                        if(UserId != 0)
                        ds.Tables[0].Rows[(ds.Tables[0].Rows.Count) - 1]["status"] = 1;
                        else //admin user
                            ds.Tables[0].Rows[m]["status"] = 1;
                    }

                    DataView dv = ds.Tables[0].DefaultView;
                    dv.Sort = "status DESC, assigned_time ASC";// "assigned_time asc";
                    DataTable dt = dv.ToTable();

                    if (dt.Columns.Count == 1)
                    {
                        var lists = dt.Rows[0][0].ToString();
                        return JsonConvert.SerializeObject(lists);
                    }

                    var list = (from DataRow row in dt.Rows

                                select new
                                {
                                    Id = Convert.ToInt32(row["Id"].ToString()),
                                    BatchDate = (row["batch_date"].ToString()).Replace("-", "/").Replace("00:00:00", ""),
                                    //String.Format("{0:dd/MM/yyyy}", row["batch_date"]).Replace("-","/"), //String.Format("{0:dd/MM/yyyy}", row["batch_date"].ToString().Replace("00:00:00", "")),
                                    Retailer = row["Retailer"].ToString(),
                                    Region = row["region_code"].ToString(),
                                    region_id = Convert.ToInt32(row["region"].ToString()),
                                    Market_Dept = Convert.ToInt32(row["Market/Dept"].ToString()),
                                    CreatedDate = Convert.ToDateTime(row["CreatedDate"].ToString()),
                                    P_G = Convert.ToInt32(row["P&G"].ToString()),
                                    Colors = Convert.ToInt32(row["Colors"].ToString()),
                                    Sizes = Convert.ToInt32(row["Sizes"].ToString()),
                                    Category = Convert.ToInt32(row["Category"].ToString()),
                                    SubCategory = Convert.ToInt32(row["SubCategory"].ToString()),
                                    Taxonomy = Convert.ToInt32(row["Taxonomy"].ToString()),
                                    Brands = Convert.ToInt32(row["Brands"].ToString()),
                                    Materials = Convert.ToInt32(row["Materials"].ToString()),
                                    OOS = Convert.ToInt32(row["OOS"].ToString()),
                                    Total = Convert.ToInt32(row["Total"].ToString()),
                                    StartTime = row["StartTime"].ToString(),
                                    EndTime = row["EndTime"].ToString(),
                                    Duriation = row["Duriation"].ToString(),
                                    Analyst = row["Analyst"].ToString(),
                                    Remarks = row["Remarks"].ToString(),
                                    UserId = Convert.ToInt32(row["UserId"].ToString()),
                                    filename = row["filename"].ToString(),
                                    status = row["status"].ToString(),
                                    workeddate = (row["assigned_time"].ToString()).Split(' ')[0].Replace("-", "/")

                                }).ToList();

                    

                    var message = "";
                    if (ds.Tables.Count > 1)
                    {
                        message = ds.Tables[1].Rows[0][0].ToString();
                    }

                    var result = new { message, list };
                    return JsonConvert.SerializeObject(result);
                    //   return Json(result,  JsonRequestBehavior.AllowGet);
                    //return JsonConvert.SerializeObject(list);
                }
                catch (Exception e)
                {
                    var message = "error: " + e.Message.ToString();
                    var result = new { message };
                    return JsonConvert.SerializeObject(result);
                    //return JsonConvert.SerializeObject("error: " + e.Message.ToString());
                }
            }
        }
        [Route("api/HomeAPI/GetMasterdetailsInit")]
        public string GetMasterdetailsInit(string CreatedDate, int UserId, int? region_id = 0)
        {
            using (PEPITASEntities db = new PEPITASEntities())
            {
                try
                {
                    CreatedDate = CreatedDate.Split('/')[2] + "-" + CreatedDate.Split('/')[1] + "-" + CreatedDate.Split('/')[0];
                    DataTable dt1 = new DataTable();
                    using (SqlConnection con = new SqlConnection(getConnection())) //basil
                    {
                        using (SqlCommand cmd = new SqlCommand())
                        {
                            con.Open();
                           // cmd.CommandText = "usp_retailerallocation";
                            cmd.CommandText = "Sp_getmaster_details";
                            cmd.Parameters.AddWithValue("@CreatedDate", CreatedDate);
                            cmd.Parameters.AddWithValue("@UserId", UserId);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Connection = con;
                            SqlDataAdapter da = new SqlDataAdapter(cmd);
                            da.Fill(dt1);
                            con.Close();
                        }
                    }
                    
                    if(UserId==0)
                    {
                        for(int m=0;m<dt1.Rows.Count;m++)
                        {
                            dt1.Rows[m]["status"] = 1;
                        }
                    }
                    if (dt1.Rows.Count > 0 && UserId != 0)
                        dt1.Rows[(dt1.Rows.Count) - 1]["status"] = 1;
                    DataView dv = dt1.DefaultView;
                    dv.Sort = "status DESC, assigned_time ASC";// , StartTime ASC "assigned_time asc";
                    DataTable dt = dv.ToTable();

                    if (dt.Columns.Count == 1)
                    {
                        var lists = dt.Rows[0][0].ToString();
                        return JsonConvert.SerializeObject(lists);
                    }
                    var list = (from DataRow row in dt.Rows

                                select new
                                {
                                    Id = Convert.ToInt32(row["Id"].ToString()),
                                    BatchDate = (row["batch_date"].ToString()).Replace("-", "/").Replace("00:00:00", ""),//String.Format("{0:dd/MM/yyyy}", row["batch_date"]).Replace("-", "/"), //String.Format("{0:dd/MM/yyyy}", row["batch_date"].ToString().Replace("00:00:00", "")),
                                    Retailer = row["Retailer"].ToString(),
                                    Region = row["region_code"].ToString(),
                                    region_id = Convert.ToInt32(row["region"].ToString()),
                                    Market_Dept = Convert.ToInt32(row["Market_Dept"].ToString()),
                                    CreatedDate = Convert.ToDateTime(row["CreatedDate"].ToString()),
                                    P_G = Convert.ToInt32(row["P_G"].ToString()),
                                    Colors = Convert.ToInt32(row["Colors"].ToString()),
                                    Sizes = Convert.ToInt32(row["Sizes"].ToString()),
                                    Category = Convert.ToInt32(row["Category"].ToString()),
                                    SubCategory = Convert.ToInt32(row["SubCategory"].ToString()),
                                    Taxonomy = Convert.ToInt32(row["Taxonomy"].ToString()),
                                    Brands = Convert.ToInt32(row["Brands"].ToString()),
                                    Materials = Convert.ToInt32(row["Materials"].ToString()),
                                    OOS = Convert.ToInt32(row["OOS"].ToString()),
                                    Total = Convert.ToInt32(row["Total"].ToString()),
                                    StartTime = row["StartTime"].ToString(),
                                    EndTime = row["EndTime"].ToString(),
                                    Duriation = row["Duriation"].ToString(),
                                    Analyst = row["Analyst"].ToString(),
                                    Remarks = row["Remarks"].ToString(),
                                    UserId = Convert.ToInt32(row["UserId"].ToString()),
                                    filename = row["filename"].ToString(),
                                    status = row["status"].ToString(),
                                    workeddate = (row["assigned_time"].ToString()).Split(' ')[0].Replace("-", "/")
                                }).ToList();

                    return JsonConvert.SerializeObject(list);
                }
                catch (Exception e)
                {
                    return JsonConvert.SerializeObject("error: " + e.Message.ToString());
                }
            }

        }
        public DataTable ToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);

            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Defining type of data column gives proper data table 
                var type = (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>) ? Nullable.GetUnderlyingType(prop.PropertyType) : prop.PropertyType);
                //Setting column names as Property names
                dataTable.Columns.Add(prop.Name, type);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    //inserting property values to datatable rows
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            //put a breakpoint here and check datatable
            return dataTable;
        }
        [Route("api/HomeAPI/GetpepitasentryDeletedetails")]
        public string GetpepitasentryDeletedetails(string CreatedDate, int RoleId, int UserId)
        {

            using (PEPITASEntities db = new PEPITASEntities())
            {
                try
                {
                    CreatedDate = CreatedDate.Split('/')[1] + "/" + CreatedDate.Split('/')[0] + "/" + CreatedDate.Split('/')[2];
                    DataTable dt1 = new DataTable();
                    if (RoleId != 3)
                        UserId = 0;
                    using (SqlConnection con = new SqlConnection(getConnection())) //basil
                    {
                        using (SqlCommand cmd = new SqlCommand())
                        {
                            con.Open();
                            cmd.CommandText = "usp_getdeleteretailers";
                            cmd.Parameters.AddWithValue("@CreatedDate", CreatedDate);
                            cmd.Parameters.AddWithValue("@userid", UserId);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Connection = con;
                            SqlDataAdapter da = new SqlDataAdapter(cmd);
                            da.Fill(dt1);
                            con.Close();
                        }
                    }
                    if (dt1.Rows.Count > 0)
                        dt1.Rows[dt1.Rows.Count - 1]["StartTime"] = get_currenttime();


                    DataView dv = dt1.DefaultView;
                    dv.Sort = "assigned_time ASC, StartTime ASC";//"assigned_time asc";
                    DataTable dt = dv.ToTable();

                    if (dt.Columns.Count == 1)
                    {
                        var lists = dt.Rows[0][0].ToString();
                        return JsonConvert.SerializeObject(lists);
                    }
                    var list = (from DataRow row in dt.Rows

                                select new
                                {
                                    Id = Convert.ToInt32(row["Id"].ToString()),
                                    BatchDate = String.Format("{0:dd/MM/yyyy}", row["batch_date"].ToString().Replace("00:00:00", "")),
                                    Retailer = row["Retailer"].ToString(),
                                    Region = row["region_code"].ToString(),
                                    region_id = Convert.ToInt32(row["region"].ToString()),
                                    Market_Dept = Convert.ToInt32(row["Market/Dept"].ToString()),
                                    CreatedDate = Convert.ToDateTime(row["CreatedDate"].ToString()),
                                    P_G = Convert.ToInt32(row["P&G"].ToString()),
                                    Colors = Convert.ToInt32(row["Colors"].ToString()),
                                    Sizes = Convert.ToInt32(row["Sizes"].ToString()),
                                    Category = Convert.ToInt32(row["Category"].ToString()),
                                    SubCategory = Convert.ToInt32(row["SubCategory"].ToString()),
                                    Taxonomy = Convert.ToInt32(row["Taxonomy"].ToString()),
                                    Brands = Convert.ToInt32(row["Brands"].ToString()),
                                    Materials = Convert.ToInt32(row["Materials"].ToString()),
                                    OOS = Convert.ToInt32(row["OOS"].ToString()),
                                    Total = Convert.ToInt32(row["Total"].ToString()),
                                    StartTime = row["StartTime"].ToString(),
                                    EndTime = row["EndTime"].ToString(),
                                    Duriation = row["Duriation"].ToString(),
                                    Remarks = row["Remarks"].ToString(),
                                    UserId = Convert.ToInt32(row["UserId"].ToString()),
                                    Access = row["access"],
                                    UserName = Convert.ToString(row["user_name"].ToString())
                                }).ToList();

                    return JsonConvert.SerializeObject(list);
                }
                catch (Exception e)
                {
                    return JsonConvert.SerializeObject("error: " + e.Message.ToString());
                }
            }


        }
        [HttpGet]
        [Route("api/HomeAPI/Movedeletededretailer")]
        public string Movedeletededretailer(int Id)
        {

            using (PEPITASEntities db = new PEPITASEntities())
            {
                try
                {
                    using (SqlConnection con = new SqlConnection(getConnection())) //basil
                    {
                        using (SqlCommand cmd = new SqlCommand())
                        {

                            var undoItem = db.Pip_MasterDetails.Where(x => x.Id == Id).SingleOrDefault();
                            string retailer = undoItem.Retailer.ToString();
                            var removeticket = db.Pip_Ticket_details.Where(x => x.Retailer == retailer && x.is_active == true).SingleOrDefault();
                            DateTime now = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, INDIAN_ZONE);
                            if (undoItem != null)
                            {
                                undoItem.Deleted = false;
                                undoItem.status = 1;
                                undoItem.StartTime = "00:00";
                                undoItem.EndTime = "00:00";
                                undoItem.Duriation = "00:00";
                                undoItem.assigned_time = now;
                                db.Entry(undoItem).State = System.Data.Entity.EntityState.Modified;
                                db.SaveChanges();
                            }
                            if (removeticket != null)
                            {
                                removeticket.is_active = false;
                                db.Entry(removeticket).State = System.Data.Entity.EntityState.Modified;
                                db.SaveChanges();
                            }
                        }
                    }
                    return "Successfully moved";
                }
                catch (Exception e)
                {
                    return JsonConvert.SerializeObject("error: " + e.Message.ToString());
                }
            }


        }



        //public string RemoveMasterDetails(List<Pip_MasterDetails> pip)
        //{
        //    try
        //    {

        //        for (int i = 0; i < pip.Count(); i++)
        //        {
        //            var RemoveItem = db.Pip_MasterDetails.Find(pip[i].Id);
        //            if (RemoveItem != null)
        //            {
        //                RemoveItem.Deleted = true;
        //                db.Entry(RemoveItem).State = System.Data.Entity.EntityState.Modified;
        //                db.SaveChanges();
        //            }
        //        }
        //        return "Pepitas Details Deleted Successfully";
        //    }

        //    catch (Exception e)
        //    {
        //        return "Error";
        //    }
        //}

        //[AcceptVerbs("GET", "DELETE")]   
        //[Route("api/HomeAPI/DeleteMasterdetails")]
        //public string RemoveMasterDetails(int id)
        //{
        //    try
        //    {

        //        if (id != null)
        //        {
        //            var RemoveItem = db.Pip_MasterDetails.Find(id);
        //            if (RemoveItem != null)
        //            {
        //                RemoveItem.Deleted = true;
        //                db.Entry(RemoveItem).State = System.Data.Entity.EntityState.Modified;
        //                db.SaveChanges();
        //            }
        //        }
        //        return "Pepitas Details Deleted Successfully";
        //    }

        //    catch (Exception e)
        //    {
        //        return "Error";
        //    }
        //}
        //
        [HttpPost]
        [Route("api/HomeAPI/InsertTicketdetails")]
        public string InsertTicketdetails(List<Pip_Ticket_details> obj)
        {
            try
            {

                for (int i = 0; i < obj.Count(); i++)
                {
                    Pip_Ticket_details list = new Pip_Ticket_details();
                    list.Retailer = obj[i].Retailer;
                    list.ticket_id = obj[i].ticket_id;
                    list.is_active = true;
                    db.Pip_Ticket_details.Add(list);
                    db.SaveChanges();
                }
                return "success";
            }
            catch (Exception e)
            {
                return "error: " + e.Message.ToString();
            }
        }
        public class ticketdetails
        {
            public string key { get; set; }
            public int value { get; set; }
        }

        [HttpPut]
        [Route("api/HomeAPI/DeleteMasterdetails")]
        public string RemoveMasterDetails(List<Pip_MasterDetails> pip)
        {
            try
            {

                for (int i = 0; i < pip.Count(); i++)
                {
                    int idval = pip[i].Id;
                    var RemoveItem = db.Pip_MasterDetails.Where(x => x.Id == idval).FirstOrDefault();
                    if (RemoveItem != null)
                    {
                        RemoveItem.Deleted = true;
                        //db.Entry(RemoveItem).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                    }
                }
                return "success";
            }
            catch (Exception e)
            {
                return "error: " + e.Message.ToString();
            }
        }

        [HttpGet]
        [Route("api/Pepitas/Export/{CreatedDate}/{RoleId}/{UserId}")]
        //[Route("api/HomeAPI/Pepitas/Export/{CreatedDate}/{RoleId}/{UserId}")]
        public string UserTaggedProducts(string CreatedDate, int RoleId, int UserId)
        {
            using (PEPITASEntities db = new PEPITASEntities())
            {
                //CreatedDate = CreatedDate.Split('/')[1] + "/" + CreatedDate.Split('/')[0] + "/" + CreatedDate.Split('/')[2];
                List<Sp_getmaster_details_Result> Pip_MasterDetail = new List<Sp_getmaster_details_Result>();
                if (RoleId == 3)
                    Pip_MasterDetail = db.Sp_getmaster_details(CreatedDate, UserId).ToList();
                else
                    Pip_MasterDetail = db.Sp_getmaster_details(CreatedDate, 0).ToList();
                var list = Pip_MasterDetail.Select(u => new
                {
                    Analyst = db.user_master.Where(x => x.user_id == u.UserId).FirstOrDefault().employee_name,
                    Retailer = u.Retailer,
                    Region = db.region_list.Where(x => x.region_id == u.region).Select(x => x.region_code).FirstOrDefault(),
                    Market_Dept = u.Market_Dept,
                    Category = u.Category,
                    SubCategory = u.SubCategory,
                    Taxonomy = u.Taxonomy,
                    Brands = u.Brands,
                    Materials = u.Materials,
                    P_G = u.P_G,
                    Colors = u.Colors,
                    Sizes = u.Sizes,
                    OOS = u.OOS,
                    Total = u.Total,
                    StartTime = u.StartTime,
                    EndTime = u.EndTime,
                    Duriation = u.Duriation,
                    Remarks = u.Remarks,
                    BatchDate = String.Format("{0:dd/MM/yyyy}", u.BatchDate),
                    workeddate = u.assigned_time
                }).ToList();

                return JsonConvert.SerializeObject(list);
            }
        }

        [HttpPost]
        [Route("api/HomeAPI/UploadExcel")]
        public string UploadExcel()
        {
            string fileSavePath = "";
            OleDbConnection conn = null;
            DataSet ds = new DataSet();

            if (System.Web.HttpContext.Current.Request.Files.AllKeys.Any())
            {
                var httpPostedFile = System.Web.HttpContext.Current.Request.Files["UploadExcel"];
                if (httpPostedFile != null)
                {
                    if (!Directory.Exists(System.Web.HttpContext.Current.Server.MapPath("~/TempFiles")))
                        Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath("~/TempFiles"));
                    fileSavePath = Path.Combine(System.Web.HttpContext.Current.Server.MapPath("~/TempFiles"), httpPostedFile.FileName);
                    httpPostedFile.SaveAs(fileSavePath);
                }

                try
                {
                    conn = new OleDbConnection(@"Provider=Microsoft.ACE.OLEDB.12.0;" + @"Data Source=" + fileSavePath + ";" + @"Extended Properties=""Excel 12.0 Macro;HDR=Yes""");
                    conn.Open();
                    OleDbCommand cmd = new OleDbCommand(@"Select * From [Pepitas$]", conn);
                    OleDbDataAdapter da = new OleDbDataAdapter(cmd);
                    da.Fill(ds);
                }
                catch (Exception e)
                {
                    return e.Message.ToString();
                }
                finally
                {
                    if (conn != null)
                        conn.Close();
                }
                if (ds.Tables.Count > 0)
                {
                    using (PEPITASEntities db = new PEPITASEntities())
                    {
                        var list = (from DataRow dr in ds.Tables[0].Rows
                                    select new
                                    {
                                        Id = 0,
                                        BatchDate = dr["BatchDate"].ToString(),
                                        Retailer = dr["Retailer"].ToString(),
                                        regionlist = db.MRG_Regionlist(1).ToList(),
                                        Region = "" + db.region_list.AsEnumerable().Where(x => x.region_code == dr["Region"].ToString() && x.region_delfalg == false).Select(x => x.region_id).FirstOrDefault(),
                                        Market_Dept = dr["Market"].ToString(),
                                        P_G = dr["PG"].ToString(),
                                        Colors = dr["Colors"].ToString(),
                                        Sizes = dr["Sizes"].ToString(),
                                        Category = dr["Category"].ToString(),
                                        SubCategory = dr["SubCategory"].ToString(),
                                        Taxonomy = dr["EditTaxonomy"].ToString(),
                                        Brands = dr["Brands"].ToString(),
                                        Materials = dr["Materials"].ToString(),
                                        OOS = dr["OOS"].ToString(),
                                        StartTime = dr["StartTime"].ToString(),
                                        EndTime = dr["EndTime"].ToString(),
                                        Duriation = dr["EndDuration"].ToString(),
                                        Remarks = dr["Remarks"].ToString()
                                    }).ToList();
                        return JsonConvert.SerializeObject(list);
                    }

                }

            }
            return "Success";

        }

        #endregion

        #region  Login


        [HttpPost]
        [Route("api/HomeAPI/VerifyLogin")]
        public string VerifyLogin(Login input)
        {
            LoginResult data = new LoginResult();
            PEPITASEntities conn = new PEPITASEntities();
            user_master login = null;
            try
            {
                if (conn.user_master.Where(x => x.user_name == input.Username && x.is_active == true && x.is_delete == false).FirstOrDefault() == null)
                {
                    data.Message = "Invalid Username!...";
                    data.Clear = false;
                    string RESULT = JsonConvert.SerializeObject(data);
                    return RESULT;

                }
                login = conn.user_master.Where(x => x.user_name == input.Username && x.user_password == input.Password && x.is_active == true && x.is_delete == false).FirstOrDefault();
                if (login == null)
                {
                    data.Message = "Invalid Password!...";
                    data.Clear = false;
                    string RESULT = JsonConvert.SerializeObject(data);
                    return RESULT;
                }
                else
                {
                    //HttpContext.Current.Session["UserName"] = data.UserName = login.employee_name;
                    //HttpContext.Current.Session["UserId"] = data.UserId = login.user_id;
                    data.RoleId = login.role_id;
                    data.RoleName = login.role_id == 1 ? "Super Admin" : login.role_id == 2 ? "Admin" : "Agent";
                    data.UserId = login.user_id != 0 ? login.user_id : 0;
                    data.UserName = login.employee_name != "" ? login.employee_name : "Agent";
                    data.Message = "Credential Verified!...";
                    data.Clear = true;
                }
                string returndata = JsonConvert.SerializeObject(data);
                return returndata;
            }
            catch (Exception ex)
            {
                data.Exception = "error: " + ex.ToString();
                return JsonConvert.SerializeObject(data);
            }

        }
        #endregion

        #region Change Password

        [HttpPost]
        [Route("api/HomeAPI/ChangePassword")]
        public string ChangePassword(string password, int Userid)
        {
            try
            {
                List<MRS_UserChangePassword_Result> Result = new List<MRS_UserChangePassword_Result>();
                using (PEPITASEntities db = new PEPITASEntities())
                {
                    Result = db.MRS_UserChangePassword(Userid, password).ToList();
                    return JsonConvert.SerializeObject(Result);

                }

            }
            catch (Exception e)
            {
                return JsonConvert.SerializeObject("Error :" + e.Message);
            }
        }
        #endregion

        #region  Extra Task

        [HttpGet]
        [Route("api/HomeAPI/GetExtraTasklist")]
        public string GetExtraTasklist()
        {
            using (PEPITASEntities db = new PEPITASEntities())
            {
                try
                {
                    var list = db.Extra_task_list.Where(x => x.is_active == true).OrderBy(x => x.task_type).Select(
                    x => new
                    {
                        Id = x.id,
                        task_type = x.task_type
                    }).ToList();

                    return JsonConvert.SerializeObject(list);
                }
                catch (Exception e)
                {
                    return JsonConvert.SerializeObject("Error :" + e.Message);
                }
            }
        }

        [HttpGet]
        [Route("api/HomeAPI/GetExtraTask/{CreatedDate}/{ToDate}/{UserId}")]
        public string GetExtraTask(string CreatedDate, string ToDate, int UserId)
        {
            using (PEPITASEntities db = new PEPITASEntities())
            {
                try
                {
                    CreatedDate = CreatedDate.Replace("-", "/");
                    ToDate = ToDate.Replace("-", "/");
                    DateTime dt = DateTime.ParseExact(CreatedDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    DateTime To = DateTime.ParseExact(ToDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    List<Extra_task> data = null;
                    if (UserId == 0)
                        data = db.Extra_task.Where(x => x.Task_Date >= dt && x.Task_Date <= To && x.is_deleted == false).ToList();
                    else
                        data = db.Extra_task.Where(x => x.Task_Date >= dt && x.Task_Date <= To && x.is_deleted == false && x.Analyst == UserId).ToList();
                    var list = data.Select(u => new
                    {
                        Id = u.Id,
                        Task_Date = u.Task_Date.Value.ToString("dd/MM/yyyy"),
                        Analyst = u.Analyst,
                        AgentList = db.user_master.Where(x => x.user_id == u.Analyst).Select(x => x.employee_name).FirstOrDefault(),
                        Task = u.Task,
                        Task_Description = u.Task_Description,
                        Start_Time = u.Start_Time,
                        End_Time = u.End_Time,
                        Total_Duration = u.Total_Duration,
                        TL_Comments = u.TL_Comments,
                        Created_Date = u.Created_Date,
                        Created_By = u.Created_By,
                        is_deleted = u.is_deleted
                    }).ToList();
                    return JsonConvert.SerializeObject(list);
                }
                catch (Exception e)
                {
                    return JsonConvert.SerializeObject("Error :" + e.Message);
                }
            }
        }



        [HttpPost]
        [Route("api/HomeAPI/SaveExtraTask")]
        public string SaveExtraTask(List<Extra_task> data)
        {
            using (PEPITASEntities db = new PEPITASEntities())
            {
                try
                {
                    DateTime now = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, INDIAN_ZONE);

                    for (int i = 0; i < data.Count(); i++)
                    {
                        data[i].Created_Date = now;
                        data[i].is_deleted = false;

                        if (data[i].Id == 0)
                        {
                            db.Extra_task.Add(data[i]);

                        }
                        else
                        {
                            var starttime = data[i].Start_Time; var task = data[i].Task; var description = data[i].Task_Description; var tlcomments = data[i].TL_Comments;
                            Extra_task list = db.Extra_task.Where(x => x.Start_Time == starttime && x.Task == task && x.Task_Description == description && x.TL_Comments == tlcomments).ToList().FirstOrDefault();
                            if (list.Id == 0)
                                db.Entry(data[i]).State = System.Data.Entity.EntityState.Modified;
                        }
                        db.SaveChanges();
                    }
                }
                catch (Exception e)
                {
                    return "Error :" + e.Message;
                }

            }

            return "Extra Tasks Added Successfully";
        }

        //[HttpPost]
        //[Route("api/HomeAPI/SaveExtraTask")]
        //public string SaveExtraTask(Extra_task data)
        //{
        //    using (WgsnEntities db = new WgsnEntities())
        //    {
        //        try
        //        {
        //            if (data != null)
        //            {
        //                data.is_deleted = false;
        //                if (data.Id == 0)
        //                    db.Extra_task.Add(data);
        //                else
        //                    db.Entry(data).State = System.Data.Entity.EntityState.Modified;
        //                db.SaveChanges();
        //            }

        //        }
        //        catch (Exception e)
        //        {
        //            return "Error";
        //        }
        //    }
        //    return "Extra Tasks Added Successfully";
        //}

        //[HttpPost]
        //[Route("api/HomeAPI/DeleteExtraTask")]
        //public string DeleteExtraTask(List<Extra_task> data)
        //{
        //    try
        //    {
        //        using (WgsnEntities db = new WgsnEntities())
        //        {
        //            for (int i = 0; i < data.Count(); i++)
        //            {
        //                var RemoveItem = db.Extra_task.Find(data[i].Id);
        //                if (RemoveItem != null)
        //                {
        //                    RemoveItem.is_deleted = true;
        //                    db.Entry(RemoveItem).State = System.Data.Entity.EntityState.Modified;
        //                    db.SaveChanges();
        //                }
        //            }
        //            return "Extra Tasks Deleted Successfully!";
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        return e.Message;
        //    }
        //}

        //[AcceptVerbs("GET", "DELETE")]   
        //[Route("api/HomeAPI/DeleteExtraTask")]
        //public string DeleteExtraTask(int id)
        //{
        //    try
        //    {
        //        using (WgsnEntities db = new WgsnEntities())
        //        {
        //            if (id != null)
        //            {
        //                var RemoveItem = db.Extra_task.Find(id);
        //                if (RemoveItem != null)
        //                {
        //                    RemoveItem.is_deleted = true;
        //                    db.Entry(RemoveItem).State = System.Data.Entity.EntityState.Modified;
        //                    db.SaveChanges();
        //                }
        //                return "Extra Tasks Deleted Successfully!";
        //            }
        //            return "Error";
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        return e.Message;
        //    }
        //}

        [HttpPut]
        [Route("api/HomeAPI/DeleteExtraTask")]
        public string DeleteExtraTask(List<Extra_task> data)
        {
            try
            {
                using (PEPITASEntities db = new PEPITASEntities())
                {
                    for (int i = 0; i < data.Count(); i++)
                    {
                        var RemoveItem = db.Extra_task.Find(data[i].Id);
                        if (RemoveItem != null)
                        {
                            RemoveItem.is_deleted = true;
                            db.Entry(RemoveItem).State = System.Data.Entity.EntityState.Modified;
                            db.SaveChanges();
                        }
                    }
                    return "success";
                }
            }
            catch (Exception e)
            {
                return "error: " + e.Message;
            }
        }

        //[Route("api/HomeAPI/ExtraTask/Export/{CreatedDate}/{ToDate}/{UserId}")]
        //public void ExportExtraTask(string CreatedDate, string ToDate, int UserId)
        //{
        //    using (WgsnEntities db = new WgsnEntities())
        //    {
        //        try
        //        {
        //            DateTime dt = DateTime.ParseExact(CreatedDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
        //            DateTime To = DateTime.ParseExact(ToDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
        //            List<Extra_task> data = null;
        //            if (UserId == 0)
        //                data = db.Extra_task.Where(x => x.Task_Date >= dt && x.Task_Date <= To && x.is_deleted == false).ToList();
        //            else
        //                data = db.Extra_task.Where(x => x.Task_Date >= dt && x.Task_Date <= To && x.is_deleted == false && x.Analyst == UserId).ToList();
        //            var list = data.Select(u => new
        //            {
        //                Task_Date = u.Task_Date.Value.ToString("dd/MM/yyyy"),
        //                Analyst = db.user_master.Where(x => x.user_id == u.Analyst).Select(x => x.employee_name).FirstOrDefault(),
        //                Task = u.Task,
        //                Task_Description = u.Task_Description,
        //                Start_Time = u.Start_Time,
        //                End_Time = u.End_Time,
        //                Total_Duration = u.Total_Duration,
        //                TL_Comments = u.TL_Comments
        //            }).ToList();

        //            HttpContext.Current.Response.Clear();
        //            HttpContext.Current.Response.ClearContent();
        //            HttpContext.Current.Response.ClearHeaders();
        //            HttpContext.Current.Response.Buffer = true;
        //            HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.UTF8;
        //            HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        //            HttpContext.Current.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        //            HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=" + CreatedDate + " To " + ToDate + ".xlsx");

        //            using (ExcelPackage pack = new ExcelPackage())
        //            {
        //                ExcelWorksheet ws = pack.Workbook.Worksheets.Add("Results");
        //                ws.Cells["A1"].LoadFromCollection(list, true);
        //                ws.Cells["A1"].Value = "Task Date";
        //                ws.Cells["B1"].Value = "Analyst";
        //                ws.Cells["C1"].Value = "Task";
        //                ws.Cells["D1"].Value = "Task Description";
        //                ws.Cells["E1"].Value = "Start Time";
        //                ws.Cells["F1"].Value = "End Time";
        //                ws.Cells["G1"].Value = "Total Duration";
        //                ws.Cells["H1"].Value = "TL Comments";
        //                var ms = new System.IO.MemoryStream();
        //                pack.SaveAs(ms);
        //                ms.WriteTo(HttpContext.Current.Response.OutputStream);
        //            }

        //            HttpContext.Current.Response.Flush();
        //            HttpContext.Current.Response.End();
        //        }
        //        catch (Exception e)
        //        {
        //        }
        //    }
        //}

        [Route("api/HomeAPI/ExtraTask/Export/{CreatedDate}/{ToDate}/{UserId}")]
        public string ExportExtraTask(string CreatedDate, string ToDate, int UserId)
        {
            using (PEPITASEntities db = new PEPITASEntities())
            {
                try
                {
                    DateTime dt = DateTime.ParseExact(CreatedDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    DateTime To = DateTime.ParseExact(ToDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    List<Extra_task> data = null;
                    if (UserId == 0)
                        data = db.Extra_task.Where(x => x.Task_Date >= dt && x.Task_Date <= To && x.is_deleted == false).ToList();
                    else
                        data = db.Extra_task.Where(x => x.Task_Date >= dt && x.Task_Date <= To && x.is_deleted == false && x.Analyst == UserId).ToList();
                    var list = data.Select(u => new
                    {
                        Task_Date = u.Task_Date.Value.ToString("dd/MM/yyyy"),
                        Analyst = db.user_master.Where(x => x.user_id == u.Analyst).Select(x => x.employee_name).FirstOrDefault(),
                        Task = u.Task,
                        Task_Description = u.Task_Description,
                        Start_Time = u.Start_Time,
                        End_Time = u.End_Time,
                        Total_Duration = u.Total_Duration,
                        TL_Comments = u.TL_Comments
                    }).ToList();

                    return JsonConvert.SerializeObject(list);
                }
                catch (Exception e)
                {
                    return "error: " + e.Message.ToString();
                }
            }
        }
        #endregion


        #region Country      


        [HttpGet]
        [Route("api/Country/GetRegionlist")]
        public string GetRegionlist()
        {
            using (PEPITASEntities db = new PEPITASEntities())
            {
                var result = db.region_list.Where(a => a.region_active == true).ToList();
                return JsonConvert.SerializeObject(result);
            }
        }

        [HttpPost]
        [Route("api/Country/SaveRegion")]
        public string SaveRegion(region_list reg)
        {
            string msg = "Region Added";
            using (PEPITASEntities db = new PEPITASEntities())
            {
                try
                {
                    var result = db.USP_ADD_REGION(reg.region_code, reg.region_name, reg.no_of_agent).ToList();
                    if (result[0] == "false")
                        msg = "Region Already Exist";
                    return msg;
                }
                catch (Exception e) { return e.Message; }
            }
        }

        [HttpPost]
        [Route("api/Country/RegionActiveInactive")]
        public string Region_active_Inactive(region_list reg)
        {

            using (PEPITASEntities db = new PEPITASEntities())
            {
                try
                {
                    db.USP_ActiveInactive_Region(reg.region_id.ToString(), Convert.ToInt16(reg.region_active));
                    return "Success";
                }
                catch (Exception e) { return e.Message; }
            }
        }

        #endregion

        #region Country_mapping

        [HttpGet]
        [Route("api/Country/GetAgentList")]
        public string GetAgentList()
        {
            using (PEPITASEntities db = new PEPITASEntities())
            {
                var result = db.user_master.Where(a => a.role_id == 2).ToList();
                return JsonConvert.SerializeObject(result);
            }
        }

        [HttpGet]
        [Route("api/Country/GetRegionlistMapping")]
        public string GetRegionlist_Mapping()
        {
            using (PEPITASEntities db = new PEPITASEntities())
            {


                //var result = (from R in db.region_list
                //              join C in db.Country_Agent_Map on R.region_id.ToString() equals C.region_id
                //              where R.region_active == true
                //              orderby R.region_id
                //              select new
                //     {
                //         region_id = R.region_id,
                //         region_name = R.region_name,
                //         no_of_agent = C.no_of_agent,
                //         region_code = R.region_code,
                //         region_active = R.region_active

                //     }).ToList();

                var result = db.region_list.Where(a => a.region_active == true).ToList();
                return JsonConvert.SerializeObject(result);
            }
        }

        [HttpGet]
        [Route("api/HomeAPI/GetMappinglistsexists")]
        public string GetMappinglistsexists(List<Mapping_Data_Temp> obj)
        {
            using (PEPITASEntities db = new PEPITASEntities())
            {
                try
                {
                    DataTable ds = new DataTable();
                    DateTime now = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, INDIAN_ZONE);
                    //now =now.ToString("yyyy-MM-dd");
                    string currentdate = now.ToString("yyyy-MM-dd");
                    using (SqlConnection con = new SqlConnection(getConnection())) //basil
                    {
                        using (SqlCommand cmd = new SqlCommand())
                        {
                            con.Open();
                            cmd.CommandText = "select distinct r.region_id,r.region_name,m.no_of_agent,m.agent_ids from region_list r, Mapping_Data m where r.region_id=m.region_id and convert(Date,Created_date)='" + currentdate + "'";
                            cmd.CommandType = CommandType.Text;
                            cmd.Connection = con;
                            //cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Connection = con;
                            SqlDataAdapter da = new SqlDataAdapter(cmd);
                            da.Fill(ds);
                            con.Close();
                        }
                    }
                    return JsonConvert.SerializeObject(ds);
                }
                catch (Exception e)
                {
                    //return Json(new { Success = "true", Message = e.Message }, JsonRequestBehavior.AllowGet);
                    return "Error :" + e.Message;
                }
            }
            //return Json(new { Success = "true", Message = "Pepitas Details Added Successfully" }, JsonRequestBehavior.AllowGet);
            //return "Pepitas Details Added Successfully";
        }

        [HttpPost]
        [Route("api/Country/SaveMappingData")]
        public string Save_Mapping_Data(List<Mapping_Data_Temp> obj)
        {

            using (PEPITASEntities db = new PEPITASEntities())
            {
                try
                {
                    DateTime now = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, INDIAN_ZONE);
                    //now =now.ToString("yyyy-MM-dd");
                    string currentdate = now.ToString("yyyy-MM-dd");
                    using (SqlConnection con = new SqlConnection(getConnection())) //basil
                    {
                        using (SqlCommand cmd = new SqlCommand())
                        {
                            con.Open();
                            cmd.CommandText = "delete from Mapping_Data where convert(Date,Created_date)='" + currentdate + "'";
                            cmd.CommandType = CommandType.Text;
                            cmd.Connection = con;
                            cmd.ExecuteNonQuery();
                            con.Close();
                        }
                    }

                    for (int i = 0; i < obj.Count(); i++)
                    {
                        //db.Mapping_Data.Add(obj[i]);
                        //db.SaveChanges();
                        db.USP_INSERT_Mapping_Data(obj[i].region_id, obj[i].no_of_agent, obj[i].agent_ids);

                    }
                    if (obj[0].Isreallocate == 1)
                    {
                        db.USP_REALLOCATE();
                    }
                }
                catch (Exception e)
                {
                    //return Json(new { Success = "true", Message = e.Message }, JsonRequestBehavior.AllowGet);
                    return "Error :" + e.Message;
                }
            }
            //return Json(new { Success = "true", Message = "Pepitas Details Added Successfully" }, JsonRequestBehavior.AllowGet);
            return "Pepitas Details Added Successfully";
        }

        #endregion

    }
}