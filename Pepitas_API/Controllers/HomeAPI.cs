﻿using Newtonsoft.Json;
using OfficeOpenXml;
using Pepitas_API.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using Wgsn.Models;

namespace Pepitas_API.Controllers
{
    public class HomeAPI : ApiController
    {
        #region Variable Declaration
        PEPITASEntities db = new PEPITASEntities();
        #endregion

      #region  Pepitas Entry

        [Route("api/HomeAPI/GetAgentName")]
        public List<user_master> GetAgentName()
        {
            List<user_master> result = db.user_master.Where(x => x.role_id == 3 && x.is_active == true && x.is_delete == false).ToList();
            return result;
        }

        [Route("api/HomeAPI/Test")]
        public string TestMethod(string Text)
        {
            return Text;
        }

        [HttpPost]
        [Route("api/HomeAPI/SaveMasterDetails")]
        public string SaveMasterDetails(List<Pip_MasterDetails> pip)
        {

            try
            {
                for (int i = 0; i < pip.Count(); i++)
                {
                    pip[i].Deleted = false;
                    if (pip[i].Id == 0)
                        db.Pip_MasterDetails.Add(pip[i]);
                    else
                        db.Entry(pip[i]).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                }
                return "Pepitas Details Added Successfully";
            }

            catch (Exception e)
            {
                return "Error :"+e.Message;
            }
        }

        [Route("api/HomeAPI/GetMasterdetails")]
        public string GetMasterdetails(string CreatedDate, int RoleId, int UserId)
        {

            CreatedDate = CreatedDate.Split('/')[1] + "/" + CreatedDate.Split('/')[0] + "/" + CreatedDate.Split('/')[2];
            //DateTime CreateDate = Convert.ToDateTime(CreatedDate);
            List<Sp_getmaster_details_Result> Pip_MasterDetail = new List<Sp_getmaster_details_Result>();
            if (RoleId == 3)
                Pip_MasterDetail = db.Sp_getmaster_details(CreatedDate, UserId).ToList();
            else
                Pip_MasterDetail = db.Sp_getmaster_details(CreatedDate, 0).ToList();
            var list = Pip_MasterDetail.Select(u => new
            {
                Id = u.Id,
                BatchDate = String.Format("{0:dd/MM/yyyy}", u.BatchDate),
                Retailer = u.Retailer,
                regionlist = db.MRG_Regionlist(1).ToList(),
                Region = u.region.ToString(),
                Market_Dept = u.Market_Dept,
                CreatedDate = u.CreatedDate,
                P_G = u.P_G,
                Colors = u.Colors,
                Sizes = u.Sizes,
                Category = u.Category,
                SubCategory = u.SubCategory,
                Taxonomy = u.Taxonomy,
                Brands = u.Brands,
                Materials = u.Materials,
                OOS = u.OOS,
                Total = u.Total,
                StartTime = u.StartTime,
                EndTime = u.EndTime,
                Duriation = u.Duriation,
                Analyst = u.Analyst,
                Remarks = u.Remarks,
                UserId = u.UserId
            }).ToList();

           return JsonConvert.SerializeObject(list);


        }


        public string RemoveMasterDetails(List<Pip_MasterDetails> pip)
        {
            try
            {

                for (int i = 0; i < pip.Count(); i++)
                {
                    var RemoveItem = db.Pip_MasterDetails.Find(pip[i].Id);
                    if (RemoveItem != null)
                    {
                        RemoveItem.Deleted = true;
                        db.Entry(RemoveItem).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                    }
                }
                return "Pepitas Details Deleted Successfully";
            }

            catch (Exception e)
            {
                return "Error :"+e.Message;
            }
        }

        [Route("Pepitas/Export/{CreatedDate}/{RoleId}/{UserId}")]
        public void UserTaggedProducts(string CreatedDate, int RoleId, int UserId)
        {
            using (PEPITASEntities db = new PEPITASEntities())
            {
                //CreatedDate = CreatedDate.Split('/')[1] + "/" + CreatedDate.Split('/')[0] + "/" + CreatedDate.Split('/')[2];
                List<Sp_getmaster_details_Result> Pip_MasterDetail = new List<Sp_getmaster_details_Result>();
                if (RoleId == 3)
                    Pip_MasterDetail = db.Sp_getmaster_details(CreatedDate, UserId).ToList();
                else
                    Pip_MasterDetail = db.Sp_getmaster_details(CreatedDate, 0).ToList();
                var list = Pip_MasterDetail.Select(u => new
                {
                    Analyst = db.user_master.Where(x => x.user_id == u.UserId).FirstOrDefault().employee_name,
                    Retailer = u.Retailer,
                    Region = db.region_list.Where(x => x.region_id == u.region).Select(x => x.region_code).FirstOrDefault(),
                    Market_Dept = u.Market_Dept,
                    Category = u.Category,
                    SubCategory = u.SubCategory,
                    Taxonomy = u.Taxonomy,
                    Brands = u.Brands,
                    Materials = u.Materials,
                    P_G = u.P_G,
                    Colors = u.Colors,
                    Sizes = u.Sizes,
                    OOS = u.OOS,
                    StartTime = u.StartTime,
                    EndTime = u.EndTime,
                    Duriation = u.Duriation,
                    Remarks = u.Remarks,
                    BatchDate = String.Format("{0:dd/MM/yyyy}", u.BatchDate),
                }).ToList();
                HttpContext.Current.Response.Clear();
                HttpContext.Current.Response.ClearContent();
                HttpContext.Current.Response.ClearHeaders();
                HttpContext.Current.Response.Buffer = true;
                HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.UTF8;
                HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                HttpContext.Current.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=" + CreatedDate + ".xlsx");
                //CommonMethod mthod = new CommonMethod();
                //var dt = mthod.ToDataTable(data);
                using (ExcelPackage pack = new ExcelPackage())
                {
                    ExcelWorksheet ws = pack.Workbook.Worksheets.Add("Results");
                    ws.Cells["A1"].LoadFromCollection(list, true);
                    ws.Cells["A1"].Value = "Analyst";
                    ws.Cells["B1"].Value = "Retailer";
                    ws.Cells["C1"].Value = "Region";
                    ws.Cells["D1"].Value = "Market/Dept";
                    ws.Cells["E1"].Value = "Category";
                    ws.Cells["F1"].Value = "Sub Category";
                    ws.Cells["G1"].Value = "Edit Taxonomy";
                    ws.Cells["H1"].Value = "Brands";
                    ws.Cells["I1"].Value = "Materials";
                    ws.Cells["J1"].Value = "P & G";
                    ws.Cells["K1"].Value = "Colors";
                    ws.Cells["L1"].Value = "Sizes";
                    ws.Cells["M1"].Value = "OOS";
                    ws.Cells["N1"].Value = "Start";
                    ws.Cells["O1"].Value = "End";
                    ws.Cells["P1"].Value = "End durations (min)";
                    ws.Cells["Q1"].Value = "Remarks";
                    ws.Cells["R1"].Value = "Batch Date";
                    var ms = new System.IO.MemoryStream();
                    pack.SaveAs(ms);
                    ms.WriteTo(HttpContext.Current.Response.OutputStream);
                }

                HttpContext.Current.Response.Flush();
                HttpContext.Current.Response.End();

            }
        }

        [HttpPost]
        [Route("api/HomeAPI/UploadExcel")]
        public string UploadExcel()
        {
            string fileSavePath = "";
            OleDbConnection conn = null;
            DataSet ds = new DataSet();

            if (System.Web.HttpContext.Current.Request.Files.AllKeys.Any())
            {
                var httpPostedFile = System.Web.HttpContext.Current.Request.Files["UploadExcel"];
                if (httpPostedFile != null)
                {
                    if (!Directory.Exists(System.Web.HttpContext.Current.Server.MapPath("~/TempFiles")))
                        Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath("~/TempFiles"));
                    fileSavePath = Path.Combine(System.Web.HttpContext.Current.Server.MapPath("~/TempFiles"), httpPostedFile.FileName);
                    httpPostedFile.SaveAs(fileSavePath);
                }

                try
                {
                    conn = new OleDbConnection(@"Provider=Microsoft.ACE.OLEDB.12.0;" + @"Data Source=" + fileSavePath + ";" + @"Extended Properties=""Excel 12.0 Macro;HDR=Yes""");
                    conn.Open();
                    OleDbCommand cmd = new OleDbCommand(@"Select * From [Pepitas$]", conn);
                    OleDbDataAdapter da = new OleDbDataAdapter(cmd);
                    da.Fill(ds);
                }
                catch (Exception e)
                {
                    return e.Message.ToString();
                }
                finally
                {
                    if (conn != null)
                        conn.Close();
                }
                if (ds.Tables.Count > 0)
                {
                    using (PEPITASEntities db = new PEPITASEntities())
                    {
                        var list = (from DataRow dr in ds.Tables[0].Rows
                                    select new
                                    {
                                        Id = 0,
                                        BatchDate = dr["BatchDate"].ToString(),
                                        Retailer = dr["Retailer"].ToString(),
                                        regionlist = db.MRG_Regionlist(1).ToList(),
                                        Region = "" + db.region_list.AsEnumerable().Where(x => x.region_code == dr["Region"].ToString() && x.region_delfalg == false).Select(x => x.region_id).FirstOrDefault(),
                                        Market_Dept = dr["Market"].ToString(),
                                        P_G = dr["PG"].ToString(),
                                        Colors = dr["Colors"].ToString(),
                                        Sizes = dr["Sizes"].ToString(),
                                        Category = dr["Category"].ToString(),
                                        SubCategory = dr["SubCategory"].ToString(),
                                        Taxonomy = dr["EditTaxonomy"].ToString(),
                                        Brands = dr["Brands"].ToString(),
                                        Materials = dr["Materials"].ToString(),
                                        OOS = dr["OOS"].ToString(),
                                        StartTime = dr["StartTime"].ToString(),
                                        EndTime = dr["EndTime"].ToString(),
                                        Duriation = dr["EndDuration"].ToString(),
                                        Remarks = dr["Remarks"].ToString()
                                    }).ToList();
                        return JsonConvert.SerializeObject(list);
                    }

                }

            }
            return "Success";

        }

        #endregion

        #region  Login
       

        [HttpPost]
        [Route("api/HomeAPI/VerifyLogin")]
        public string VerifyLogin(Login input)
        {
            LoginResult data = new LoginResult();
            PEPITASEntities conn = new PEPITASEntities();
            user_master login = null;
            if (conn.user_master.Where(x => x.user_name == input.Username && x.is_active == true && x.is_delete == false).FirstOrDefault() == null)
            {
                data.Message = "Invalid Username!...";
                data.Clear = false;
                string RESULT = JsonConvert.SerializeObject(data);
                return RESULT;
                
            }
            login = conn.user_master.Where(x => x.user_name == input.Username && x.user_password == input.Password && x.is_active == true && x.is_delete == false).FirstOrDefault();
            if (login == null)
            {
                data.Message = "Invalid Password!...";
                data.Clear = false;
                string RESULT = JsonConvert.SerializeObject(data);
                return RESULT;
            }
            else
            {
                HttpContext.Current.Session["UserName"] = data.UserName = login.employee_name;
                HttpContext.Current.Session["UserId"] = data.UserId = login.user_id;
                data.RoleId = login.role_id;
                data.RoleName = login.role_id == 1 ? "Super Admin" : login.role_id == 2 ? "Admin" : "Agent";
                data.Message = "Credential Verified!...";
                data.Clear = true;
            }
             string returndata = JsonConvert.SerializeObject(data);
             return returndata;

        }
        #endregion

        #region Change Password
        
        [HttpPost]
        [Route("api/HomeAPI/ChangePassword")]
        public string ChangePassword(string password, int Userid)
        {
            try
            {
                List<MRS_UserChangePassword_Result> Result = new List<MRS_UserChangePassword_Result>();
                using (PEPITASEntities db = new PEPITASEntities())
                {
                    Result = db.MRS_UserChangePassword(Userid, password).ToList();
                    return JsonConvert.SerializeObject(Result);
                    
                }
                
            }
            catch (Exception e)
            {
                return JsonConvert.SerializeObject("Error :"+e.Message);
            }
        }
        #endregion

        #region  Extra Task
        
        [HttpGet]
        [Route("api/HomeAPI/GetExtraTask")]
        public string GetExtraTask(string CreatedDate, string ToDate, int UserId)
        {
            using (PEPITASEntities db = new PEPITASEntities())
            {
                try
                {
                    DateTime dt = DateTime.ParseExact(CreatedDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    DateTime To = DateTime.ParseExact(ToDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    List<Extra_task> data = null;
                    if (UserId == 0)
                        data = db.Extra_task.Where(x => x.Task_Date >= dt && x.Task_Date <= To && x.is_deleted == false).ToList();
                    else
                        data = db.Extra_task.Where(x => x.Task_Date >= dt && x.Task_Date <= To && x.is_deleted == false && x.Analyst == UserId).ToList();
                    var list = data.Select(u => new
                    {
                        Id = u.Id,
                        Task_Date = u.Task_Date.Value.ToString("dd/MM/yyyy"),
                        Analyst = u.Analyst,
                        AgentList = db.user_master.Where(x => x.user_id == u.Analyst).Select(x => x.employee_name).FirstOrDefault(),
                        Task = u.Task,
                        Task_Description = u.Task_Description,
                        Start_Time = u.Start_Time,
                        End_Time = u.End_Time,
                        Total_Duration = u.Total_Duration,
                        TL_Comments = u.TL_Comments,
                        Created_Date = u.Created_Date,
                        Created_By = u.Created_By,
                        is_deleted = u.is_deleted
                    }).ToList();
                    return JsonConvert.SerializeObject(list);
                }
                catch (Exception e)
                {
                    return JsonConvert.SerializeObject("Error :"+e.Message);
                }
            }
        }

        [HttpPost]
        [Route("api/HomeAPI/SaveExtraTask")]
        public string SaveExtraTask(List<Extra_task> data)
        {
            using (PEPITASEntities db = new PEPITASEntities())
            {
                try
                {
                    for (int i = 0; i < data.Count(); i++)
                    {
                        data[i].is_deleted = false;
                        if (data[i].Id == 0)
                            db.Extra_task.Add(data[i]);
                        else
                            db.Entry(data[i]).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                    }
                }
                catch (Exception e)
                {
                    return "Error :" + e.Message;
                }
            }
            return "Extra Tasks Added Successfully";
        }

        [HttpPost]
        [Route("api/HomeAPI/DeleteExtraTask")]
        public string DeleteExtraTask(List<Extra_task> data)
        {
            try
            {
                using (PEPITASEntities db = new PEPITASEntities())
                {
                    for (int i = 0; i < data.Count(); i++)
                    {
                        var RemoveItem = db.Extra_task.Find(data[i].Id);
                        if (RemoveItem != null)
                        {
                            RemoveItem.is_deleted = true;
                            db.Entry(RemoveItem).State = System.Data.Entity.EntityState.Modified;
                            db.SaveChanges();
                        }
                    }
                    return "Extra Tasks Deleted Successfully!";
                }
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        [Route("api/HomeAPI/ExtraTask/Export/{CreatedDate}/{ToDate}/{UserId}")]
        public void ExportExtraTask(string CreatedDate, string ToDate, int UserId)
        {
            using (PEPITASEntities db = new PEPITASEntities())
            {
                try
                {
                    DateTime dt = DateTime.ParseExact(CreatedDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    DateTime To = DateTime.ParseExact(ToDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    List<Extra_task> data = null;
                    if (UserId == 0)
                        data = db.Extra_task.Where(x => x.Task_Date >= dt && x.Task_Date <= To && x.is_deleted == false).ToList();
                    else
                        data = db.Extra_task.Where(x => x.Task_Date >= dt && x.Task_Date <= To && x.is_deleted == false && x.Analyst == UserId).ToList();
                    var list = data.Select(u => new
                    {
                        Task_Date = u.Task_Date.Value.ToString("dd/MM/yyyy"),
                        Analyst = db.user_master.Where(x => x.user_id == u.Analyst).Select(x => x.employee_name).FirstOrDefault(),
                        Task = u.Task,
                        Task_Description = u.Task_Description,
                        Start_Time = u.Start_Time,
                        End_Time = u.End_Time,
                        Total_Duration = u.Total_Duration,
                        TL_Comments = u.TL_Comments
                    }).ToList();

                    HttpContext.Current.Response.Clear();
                    HttpContext.Current.Response.ClearContent();
                    HttpContext.Current.Response.ClearHeaders();
                    HttpContext.Current.Response.Buffer = true;
                    HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.UTF8;
                    HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    HttpContext.Current.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=" + CreatedDate + " To " + ToDate + ".xlsx");

                    using (ExcelPackage pack = new ExcelPackage())
                    {
                        ExcelWorksheet ws = pack.Workbook.Worksheets.Add("Results");
                        ws.Cells["A1"].LoadFromCollection(list, true);
                        ws.Cells["A1"].Value = "Task Date";
                        ws.Cells["B1"].Value = "Analyst";
                        ws.Cells["C1"].Value = "Task";
                        ws.Cells["D1"].Value = "Task Description";
                        ws.Cells["E1"].Value = "Start Time";
                        ws.Cells["F1"].Value = "End Time";
                        ws.Cells["G1"].Value = "Total Duration";
                        ws.Cells["H1"].Value = "TL Comments";
                        var ms = new System.IO.MemoryStream();
                        pack.SaveAs(ms);
                        ms.WriteTo(HttpContext.Current.Response.OutputStream);
                    }

                    HttpContext.Current.Response.Flush();
                    HttpContext.Current.Response.End();
                }
                catch (Exception e)
                {
                    var error = "Error :" + e.Message;
                }
            }
        }
        #endregion
 
    }
}